package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class BiologicsPage {
public WebDriver driver = null;
	
	public BiologicsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(className = "top-carousel")
	protected WebElement heroCarousel;
	public WebElement getHeroCarousel(){
		return heroCarousel;
	}
	
	@FindBy(className = "article-content")
	protected WebElement article;
	public WebElement getArticle(){
		return article;
	}
	
	@FindBy(xpath = "//div[2]/div/div/div/div/p[3]/a")
	protected WebElement clickHere;
	public WebElement getClickHere(){
		return clickHere;
	}
	
	@FindBy(className = "right-content-block")
	private List<WebElement> latestNews;
	public List<WebElement> getLatestNews()
	{
		return latestNews;
	}
	
	@FindBy(className = "block-resize")
	private List<WebElement> blocks;
	public List<WebElement> getBlocks()
	{
		return blocks;
	}
	
	@FindBys({
		@FindBy(className = "main-nav-links"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> mainNavLinks;
	public List<WebElement> getMainNavLinks(){
		return mainNavLinks;
	}
	
	@FindBys({
		@FindBy(className = "searchForm"),
		@FindBy(tagName = "input")
	})
	private WebElement searchTextBox;
	public WebElement getSearchTextBox(){
		return searchTextBox;
	}
	
	@FindBys({
		@FindBy(className = "searchForm"),
		@FindBy(tagName = "button")
	})
	private WebElement searchButton;
	public WebElement getSearchButton(){
		return searchButton;
	}
	//
	@FindBy(id = "global-language")
	private WebElement catalentMainPage;
	public WebElement getCatalentMainPage(){
		return catalentMainPage;
	}
	
	@FindBy(className = "logo")
	private WebElement logo;
	public WebElement getLogo(){
		return logo;
	}
	
	@FindBys({
		@FindBy(className = "links-container"),
		@FindBy(tagName = "a")
	})
	protected List<WebElement> navMenu;
	public List<WebElement> getNavMenu(){
		return navMenu;
	}
	
	@FindBy(className = "footer")
	private WebElement footer;
	public WebElement getFooter(){
		return footer;
	}
	
	@FindBy(className = "underline")
	private List<WebElement> footerlinks;
	public List<WebElement> getFooterLinks(){
		return footerlinks;
	}
	
	@FindBy(className = "facebook")
	private WebElement facebook;
	public WebElement getFacebook(){
		return facebook;
	}
	
	@FindBy(className = "twitter")
	private WebElement twitter;
	public WebElement getTwitter(){
		return twitter;
	}
	
	@FindBy(className = "linkedin")
	private WebElement linkedin;
	public WebElement getLinkedin(){
		return linkedin;
	}
	
	@FindBy(className = "youtube")
	private WebElement youtube;
	public WebElement getYoutube(){
		return youtube;
	}
	
	@FindBy(xpath = "//div[@id='livechat-eye-catcher']/a")
	private WebElement closeIcon;
	public WebElement getcloseIcon(){
		return closeIcon;
	}
	@FindBy(id = "livechat-eye-catcher-img")
	private WebElement image;
	public WebElement getImage(){
		return image;
	}
	
	@FindBys({
		@FindBy(className = "right"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> copyrightlinks;
	public List<WebElement> getCopyrightLinks(){
		return copyrightlinks;
	}
}
