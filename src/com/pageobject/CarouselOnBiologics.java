package com.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CarouselOnBiologics extends BiologicsPage {
	
	public CarouselOnBiologics(WebDriver driver) {
		super(driver);
	}
	
	public WebElement getSelf() {
		return super.heroCarousel;
	}
	
	public WebElement getFirst() throws InterruptedException {
		for(int i=0; i<3; i++){
			if(!super.heroCarousel.findElement(By.className("orange-skin")).getAttribute("style").contains("translateX(0")){
				getNext().click();
				Thread.sleep(1000);
			}
		}
		return super.heroCarousel.findElement(By.xpath("div/div[3]/div[1]"));
	}
	
	public WebElement getSecond() throws InterruptedException {
		for(int i=0; i<3; i++){
			if(!super.heroCarousel.findElement(By.className("orange-skin")).getAttribute("style").contains("translateX(-3")){
				getNext().click();
				Thread.sleep(1000);
			}
		}
		return super.heroCarousel.findElement(By.xpath("div/div[3]/div[2]"));
	}
	
	public WebElement getThird() throws InterruptedException {
		for(int i=0; i<3; i++){
			if(!super.heroCarousel.findElement(By.className("orange-skin")).getAttribute("style").contains("translateX(-6")){
				getNext().click();
				Thread.sleep(1000);
			}
		}
		return super.heroCarousel.findElement(By.xpath("div/div[3]/div[3]"));
	}

	public WebElement getPrev() {
		return super.heroCarousel.findElement(By.className("nav-prev"));
	}
	
	public WebElement getNext() {
		return super.heroCarousel.findElement(By.className("nav-next"));
	}
	
	public WebElement getPlayVideo() {
		return super.heroCarousel.findElement(By.xpath("//b/a"));
	}
	
	public WebElement getLearnMore() {
		return super.heroCarousel.findElement(By.xpath("//p[2]/b/a"));
	}
	
	public WebElement getLearnMoreOnSlide3() {
		return super.heroCarousel.findElement(By.xpath("//div[2]/div/div/div/p[2]/a"));
	}
	
	public WebElement getViewWebInar() {
		return super.heroCarousel.findElement(By.xpath("//p[3]/a"));
	}

}
