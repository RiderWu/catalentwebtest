package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class ConsumerHealthPage {
public WebDriver driver = null;
	
	public ConsumerHealthPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "top-carousel")
	protected WebElement heroCarousel;
	public WebElement getHeroCarousel(){
		return heroCarousel;
	}

	@FindBy(className = "consumer-health--title")
	protected WebElement ConsumerHealthTitle;
	public WebElement getConsumerHealthTitle(){
		return ConsumerHealthTitle;
	}
	//test
	@FindBys({
		@FindBy(className = "consumer-health"),
		@FindBy(tagName = "a")
	})
	protected List<WebElement> Items;
	public List<WebElement> getItems(){
		return Items;
	}
	@FindBy(xpath = "//div[1]/div[3]/div[2]/div/div[3]/ul/li[2]/a")
	protected WebElement NutritionalSupplements;
	public WebElement getNutritionalSupplements(){
		return NutritionalSupplements;
	}
	@FindBy(xpath = "div[3]/ul/li[3]/a")
	protected WebElement Beauty;
	public WebElement getBeauty(){
		return Beauty;
	}
	
	@FindBy(className = "article-title")
	protected WebElement ArticalTitle;
	public WebElement getArticalTitle(){
		return ArticalTitle;
	}
	
	@FindBy(className = "title")
	private List<WebElement> Titles;
	public List<WebElement> getTitles()
	{
		return Titles;
	}
}
