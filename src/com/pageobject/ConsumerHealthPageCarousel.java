package com.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class ConsumerHealthPageCarousel extends ConsumerHealthPage {

	public ConsumerHealthPageCarousel(WebDriver driver) {
		super(driver);
		
	}
	public WebElement getSelf() {
		return super.heroCarousel;
	}
	
	public WebElement getFirst() throws InterruptedException {
		for(int i=0; i<3; i++){
			if(!super.heroCarousel.findElement(By.className("orange-skin ")).getAttribute("style").contains("translateX(0")){
				getNext().click();
				Thread.sleep(1000);
			}
		}
		return super.heroCarousel.findElement(By.className("kit-middle-content"));
	}
	
	public WebElement getSecond() throws InterruptedException {
		for(int i=0; i<3; i++){
			if(!super.heroCarousel.findElement(By.className("orange-skin ")).getAttribute("style").contains("translateX(-3")){
				getNext().click();
				Thread.sleep(1000);
			}
		}
		return super.heroCarousel.findElement(By.className("kit-middle-content"));
	}
	
	public WebElement getThird() throws InterruptedException {
		for(int i=0; i<3; i++){
			if(!super.heroCarousel.findElement(By.className("orange-skin ")).getAttribute("style").contains("translateX(-6")){
				getNext().click();
				Thread.sleep(1000);
			}
		}
		return super.heroCarousel.findElement(By.className("kit-middle-content"));
	}

	public WebElement getPrev() {
		return super.heroCarousel.findElement(By.className("nav-prev"));
	}
	
	public WebElement getNext() {
		return super.heroCarousel.findElement(By.className("nav-next"));
	}
	
	public WebElement getPlayVideo() {
		return super.heroCarousel.findElements(By.className("bottom-link")).get(0);
	}
	
//	public WebElement getCloseVideo() {
//		return super.heroCarousel.findElement(By.className("video-container-close"));
//	}
	
	public WebElement getExploreLibrary() {
		return super.heroCarousel.findElements(By.className("bottom-link")).get(1);
	}
	
	public WebElement getBrowseAll() {
		return super.heroCarousel.findElements(By.className("bottom-link")).get(2);
	}
}

