package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class ConsumerHealth_ContactUs {
	 public WebDriver driver = null;
		
		public ConsumerHealth_ContactUs(WebDriver driver){
			this.driver = driver;
			PageFactory.initElements(driver, this);
	 
		}
		@FindBy(id = "firstname")
		private WebElement firstName;
		public WebElement getFirstName(){
			return firstName;
		}	
		@FindBy(id = "lastname")
		private WebElement lastName;
		public WebElement getLastName(){
			return lastName;
		}	
		@FindBy(id = "company")
		private WebElement companyName;
		public WebElement getCompanyName(){
			return companyName;
		}	
		@FindBy(id = "phone")
		private WebElement phoneNumber;
		public WebElement getPhoneNumber(){
			return phoneNumber;
		}	
		@FindBy(id = "email")
		private WebElement email;
		public WebElement getEmail(){
			return email;
		}	
		@FindBy(id = "confirmemail")
		private WebElement confirmEmail;
		public WebElement getconfirmEmail(){
			return confirmEmail;
		}	
			
		@FindBy(id = "region")
		private WebElement region;
		public WebElement getRegion(){
			return region;
		}	
		@FindBys({
			@FindBy(id = "region"),
			@FindBy(css = "#region > option")
		})
		private List<WebElement> regionList;
		public List<WebElement> getRegionList(){
			return regionList;
		}	
		@FindBy(id = "purpose")
		private WebElement purpose;
		public WebElement getPurpose(){
			return purpose;
		}	
		@FindBys({
			@FindBy(id = "purpose"),
			@FindBy(css = "#purpose > option")
		})
		private List<WebElement> purposeList;
		public List<WebElement> getPurposeList(){
			return purposeList;
		}	
		@FindBy(id = "challenge")
		private WebElement challenge;
		public WebElement getChallenge(){
			return challenge;
		}
		@FindBy(css = ".col-product-category > div")
		private List<WebElement> productCategory;
		public List<WebElement> getProductCategory(){
			return productCategory;
		}
		@FindBy(className = "submit-form")
		private WebElement submit;
		public WebElement getSubmit(){
			return submit;
		}
		@FindBy(className = "error-message")
		private WebElement errorMessage;
		public WebElement getErrorMessage(){
			return errorMessage;
		}
}
