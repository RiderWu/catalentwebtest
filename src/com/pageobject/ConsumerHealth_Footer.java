package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class ConsumerHealth_Footer {
public WebDriver driver = null;
	
	public ConsumerHealth_Footer(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(css = ".footer-main-links > a")
	private List <WebElement> footerNav;
	public List <WebElement> getFooterNav(){
		return footerNav;
	}
	@FindBy(css = ".block-right > a")
	private List <WebElement> footerShare;
	public List <WebElement> getFooterShare(){
		return footerShare;
	}
	@FindBy(css = ".right > a")
	private List <WebElement> corporate;
	public List <WebElement> getCorporate(){
		return corporate;
	}
}
