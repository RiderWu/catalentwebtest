package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class ConsumerHealth_Header {
 public WebDriver driver = null;
	
	public ConsumerHealth_Header(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
 
	}
	@FindBy(css = "div.logo-box > img")
	private WebElement consumerLogo;
	public WebElement getConsumerLogo(){
		return consumerLogo;
	}
	@FindBy(css = ".main-nav-links > a")
	private List<WebElement> navigation;
	public List<WebElement> getNavigation(){
		return navigation;
	}
	
	@FindBy(css = ".links-container > a")
	private List<WebElement> menu;
	public List<WebElement> getNavigationMenu(){
		return menu;
	}
	@FindBy(css = ".searchForm > .searchText")
	private WebElement searchBox;
	public WebElement getSearchBox(){
		return searchBox;
	}
	@FindBy(css = ".searchForm > .searchButton")
	private WebElement searchButton;
	public WebElement getSearchButton(){
		return searchButton;
	}
	
	@FindBy(css = ".type-product-item > .elem")
	private List<WebElement> locationname;
	public List<WebElement> getLocationname(){
		return locationname;
	
	}
	
	@FindBy(className = "view-all")
	private WebElement allLocation;
	public WebElement getAllLocation(){
		return allLocation;
	}
	
}
