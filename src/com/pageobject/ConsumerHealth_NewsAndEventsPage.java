package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class ConsumerHealth_NewsAndEventsPage {
	
	public WebDriver driver = null;

	public ConsumerHealth_NewsAndEventsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = ".kit-middle-content > h1")
	private WebElement hero;

	public WebElement getHero() {
		return hero;
	}
	
	@FindBy(css = ".selected > p")
	private WebElement news;

	public WebElement getNews() {
		return news;
	}

	@FindBy(xpath = "//div[3]/p")
	private WebElement events;

	public WebElement getEvents() {
		return events;
	}
	 
	@FindBy(css = ".data-content > p > a ")
	private List<WebElement> newsDetail;

	public List<WebElement> getNewsDetail() {
		return newsDetail;
	}
	
	@FindBy(xpath = "//div[3]/div/div/div/div/div/div[2]/p/a")
	private List<WebElement> eventsDetail;

	public List<WebElement> getEventsDetail() {
		return eventsDetail;
	}
	
	@FindBy(css = "p.kiwi-hidden-s > a")
	private WebElement allNews;

	public WebElement getAllNews() {
		return allNews;
	
}
	@FindBy(css = " div.tab-content.active > div.kit-content-default > div.view-all-block > p.kiwi-hidden-s > a")
	private WebElement allEvents;

	public WebElement getAllEvents() {
		return allEvents;	
}
	@FindBy(css = " .article-selector > .value")
	private WebElement upcoming;

	public WebElement getUpcoming() {
		return upcoming;	
}
	@FindBys({
		@FindBy(className = "option-list"),
		@FindBy(tagName = "a")
		})
	private List<WebElement> seventeen;

	public List<WebElement> getSeventeen() {
		return seventeen;	
}
	}