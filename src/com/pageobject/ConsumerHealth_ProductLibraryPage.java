package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ConsumerHealth_ProductLibraryPage {

	public WebDriver driver = null;
	public ConsumerHealth_ProductLibraryPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	//over the counter button
	@FindBy(className="consumer-health--items-img")
	private List<WebElement> stepBtn;
	public List<WebElement> getStepBtn(){
		return stepBtn;
	}
	
	@FindBy(className = "dale-des")
	private WebElement titel;
	public WebElement getTitle(){
		return titel;
	}
	
	//over the counter page,select all in step 2
	//*[@id="ProductList"]/div/div/div[2]/label
	@FindBy(xpath="//div[2]/label")
	//*[@id="ProductList"]/div/div/div[2]/label/span
	private WebElement selectAllBtn;
	public WebElement getSelectAllBtn(){
		return selectAllBtn;
	}
	
	//back button on step2
	@FindBy(className = "back-btn")
	private WebElement backBtn;
	public WebElement getBackBtn(){
		return backBtn;
	}
	//over the conter page ,next button in step 2
	//*[@id="ProductList"]/div/div/div[3]/a
	@FindBy(xpath="//form/div/div/div[3]/a")
	private WebElement nextBtn;
	public WebElement getNextBtn(){
		return nextBtn;
	}
	
	//the first item in result list
	//*[@id="pl_type"]/div/div[2]/div[1]/div[2]
	//*[@id="pl_type"]/div/div[2]/div[1]/div[9]
	//*[@id="pl_type"]/div/div[2]/div[1]/div[29]
	//*[@id="pl_type"]/div/div[2]/div[1]/div[42]
	@FindBy(xpath="//div[contains(@class , 'tagged-elem show')]/a/div")
	private WebElement firstItem;
	public WebElement getFirstItem(){
		return firstItem;
	}
	
	//page title on step 3
	//*[@id="contactUs"]/div[1]/div/p
	@FindBy(xpath="//form[@id='contactUs']/div/div/h2")
	private WebElement step3PageTitle;
	public WebElement getStep3PageTitle(){
		return step3PageTitle;
	}
	
	//page title
	@FindBy(className="article-content")
	private WebElement resultPageTitle;
	public WebElement getResultPageTitle(){
		return resultPageTitle;
	}
	
	//first category on filter
	@FindBy(xpath="//div[4]/div")
	private WebElement filterCategoryBtn;
	public WebElement getFilterCategoryBtn(){
		return filterCategoryBtn;
	}
	//filter item on the left
	//*[@id="pl_type"]/div/div[1]/div/div[6]/ul/li[1]
	//*[@id="pl_type"]/div/div[1]/div/div[2]/ul/li[1]
	@FindBy(xpath="//div[4]/ul/li/span")
	private WebElement firstFilterItem;
	public WebElement getFirstFilterItem(){
		return firstFilterItem;
	}
	
	//first name field on step 3
	@FindBy(id="firstname")
	private WebElement firstName;
	public WebElement getFirstName(){
		return firstName;
	}
	
	//last name field on step 3
	@FindBy(id="lastname")
	private WebElement lastName;
	public WebElement getLastName(){
		return lastName;
	}
	
	// company field on step 3
	@FindBy(id = "company")
	private WebElement company;
	public WebElement getCompany() {
		return company;
	}
	//Email field on step 3
	@FindBy(id = "email")
	private WebElement email;
	public WebElement getEmail() {
		return email;
	}
	//back button on step 3
//	@FindBy(className = "back-btn")
//	private WebElement backBtn;
//	public WebElement getBackBtn() {
//		return backBtn;
//	}
//	
	//view results button on step 3
	@FindBy(className = "submit-btn")
	private WebElement submitBtn;
	public WebElement getSubmitBtn() {
		return submitBtn;
	}
	
	//contact us CTA on results page
	///html/body/div[1]/div[3]/div[2]/div/div[3]/div/div[2]/div[2]/a
	@FindBy(xpath="//div[@class='product-contact-us']/a")
	private WebElement contactBtn;
	public WebElement getContactBtn() {
		return contactBtn;
	}
	
	//first name on contact page
	@FindBy(id = "firstname")
	private WebElement contactFirstNameBox;
	public WebElement getContactFirstNameBox() {
		return contactFirstNameBox;
	}
	@FindBy(id = "lastname")
	private WebElement contactLastNameBox;
	public WebElement getContactLastNameBox() {
		return contactLastNameBox;
	}
	@FindBy(id = "company")
	private WebElement contactCompanyBox;
	public WebElement getContactCompanyBox() {
		return contactCompanyBox;
	}
	@FindBy(id = "email")
	private WebElement contactEmailBox;
	public WebElement getContactEmailBox() {
		return contactEmailBox;
	}
	@FindBy(id = "confirmemail")
	private WebElement contactConEmailBox;
	public WebElement getContactConEmailBox() {
		return contactConEmailBox;
	}
	@FindBy(xpath = "//select[@id='region']")
	private WebElement contactRegionBox;
	public WebElement getContactRegionBox() {
		return contactRegionBox;
	}
	
	//first option on drop down
	//*[@id="region"]/option[2]
	@FindBy(xpath ="//select[@id='region']/option[@value='North America']")
	private WebElement contactRegionOptionBox;
	public WebElement getContactRegionOptionBox() {
		return contactRegionOptionBox;
	}
	
	//purpose
	@FindBy(xpath = "//select[@id='purpose']")
	private WebElement contactPurposeBox;
	public WebElement getContactPurposeBox() {
		return contactPurposeBox;
	}
	
	//purpose value
	//*[@id="CONSUMERHEALTH-EN-ProductLibrary-ContactUs-v1"]/div[2]/div[1]/div[2]/div[7]/div/div[2]/span
		@FindBy(xpath ="//select[@id='purpose']/option[@value='Considering solutions for an immediate need']")
		private WebElement contactPurposeOption;
		public WebElement getContactPurposeOption() {
	//		Select select = new Select(driver.findElement(ById.xpath("//select[@id='purpose']")));
	//		select.selectByValue("Considering solutions for an immediate need");
			return contactPurposeOption;
		}
	
	//select one primary area of interest
	//div[9]/div[2]/div[2]/div
	//@FindBy(xpath ="//div[9]/div[2]/div[2]/div")
	@FindBy(css="div.custom-radio")
	private WebElement contactCategoryOptionBox;
	public WebElement getContactCategoryOptionBox() {
		return contactCategoryOptionBox;
	}
	//submit button on contact page
	@FindBy(className ="submit-form")
	private WebElement contactSubmitBtn;
	public WebElement getContactSubmitBtn() {
		return contactSubmitBtn;
	}
	
	
	
}
