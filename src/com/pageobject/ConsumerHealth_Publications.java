package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class ConsumerHealth_Publications {
	public WebDriver driver = null;
	public ConsumerHealth_Publications(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	//title all button
	@FindBy(className="kiwi-grid")
	public WebElement filterTitle;
	public WebElement getFilterTitle(){
		return filterTitle;
	}
	
	@FindBys({
		@FindBy(className="filter-link-container"),
		@FindBy(className="filter-link")
		
		})
	private List<WebElement> filterItems;
	public List<WebElement> getFilterItems(){
		return filterItems;
	}
	
	//browse all offerings
	@FindBy(xpath="//p/a")
	public WebElement browseAllOfferings;
	public WebElement getBrowseAllOfferings(){
		return browseAllOfferings;
	}
	// search field input box
	@FindBy(id="search_text")
	public WebElement searchText;
	public WebElement getSearchText(){
		return searchText;
	}
	
	@FindBy(id="search_button")
	public WebElement searchBtn;
	public WebElement getSearchBtn(){
		return searchBtn;
	}
	
	@FindBys({
		@FindBy(className="tagged-elem-block"),
		@FindBy(tagName="a")
	})
	public List<WebElement> linkItems ;
	public List<WebElement> getLinkItems(){
		return linkItems;
	}
	//pager
	@FindBys({
		@FindBy(className="tagged-elem-nav"),
		@FindBy(className="page_selector")
	})
	public List<WebElement> pageItems ;
	public List<WebElement> getPageItems(){
		return pageItems;
	}
}
