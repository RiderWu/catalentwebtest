package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class IndexPage {
public WebDriver driver = null;
	
	public IndexPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "top-carousel")
	protected WebElement heroCarousel;
	public WebElement getHeroCarousel(){
		return heroCarousel;
	}
	
	@FindBy(className = "latest-news")
	private WebElement latestNews;
	public WebElement getLatestNews(){
		return latestNews;
	}
	
	public WebElement getlatestNewsLink(){
		return latestNews.findElement(By.tagName("a"));
	}
	
	@FindBy(className = "middle-blocks__first")
	private WebElement firstBlock;
	public WebElement getFirstBlock(){
		return firstBlock;
	}
	
	@FindBy(className = "middle-blocks__second")
	private WebElement secondBlock;
	public WebElement getSecondBlock(){
		return secondBlock;
	}
	
	@FindBy(className = "middle-blocks__third")
	private WebElement thirdBlock;
	public WebElement getThirdBlock(){
		return thirdBlock;
	}
	
	@FindBy(className = "block-1")
	private WebElement newsTitle;
	public WebElement getNewsTitle(){
		return newsTitle;
	}
	
	@FindBy(className = "data-content-col")
	private WebElement newsContent;
	public WebElement getNewsContent(){
		return newsContent;
	}
	
	@FindBy(className = "view-all")
	private WebElement newsViewAll;
	public WebElement getNewsViewAll(){
		return newsViewAll;
	}
	
	@FindBy(className = "block-2")
	private WebElement eventsTitle;
	public WebElement getEventsTitle(){
		return eventsTitle;
	}
	
	@FindBy(xpath = "//div[5]/div/div[3]/div[2]/div/div/div[2]")
	private WebElement eventsContent;
	public WebElement getEventsContent(){
		return eventsContent;
	}
	
	@FindBy(xpath = "//div[3]/div/div/div[2]/div/p/a")
	private WebElement eventsViewAll;
	public WebElement getEventsViewAll(){
		return eventsViewAll;
	}
	
	@FindBy(className = "block-3")
	private WebElement twitterTitle;
	public WebElement getTwitterTitle(){
		return twitterTitle;
	}
	
	@FindBy(className = "twitter-elem")
	private List<WebElement> twitterItems;
	public List<WebElement> getTwitterItems(){
		return twitterItems;
	}
	
	@FindBy(xpath = "//div[3]/div/div/div[3]/div/p/a")
	private WebElement twitterFollowUs;
	public WebElement getTwitterFollowUs(){
		return twitterFollowUs;
	}
}
