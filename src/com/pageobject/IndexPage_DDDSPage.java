package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class IndexPage_DDDSPage {
public WebDriver driver = null;
	
	public IndexPage_DDDSPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "(//input[@name='SearchText'])[3]")
	private WebElement searchTextBox;
	public WebElement getSearchTextBox(){
		return searchTextBox;
	}
	
	@FindBy(xpath = "(//button[@type='submit'])[2]")
	private WebElement searchButton;
	public WebElement getSearchButton(){
		return searchButton;
	}
	
	@FindBy(className = "content__explore")
	private WebElement allPhase;
	public WebElement getAllPhase(){
		return allPhase;
	}
	
	@FindBy(xpath = "//div[@id='wrap-phases']/div[2]/div/div/div/h2")
	private WebElement oralDoesForms;
	public WebElement getOralDoesForms(){
		return oralDoesForms;
	}
	
	@FindBys({
		@FindBy(className = "item_1_con"),
		@FindBy(tagName = "ul")
	})
	private List<WebElement> allItemsInOralDoesForms;
	public List<WebElement> getAllItemsInOralDoesForms()
	{
		return allItemsInOralDoesForms;
	}
	
	@FindBy(xpath = "//div[@id='wrap-phases']/div[2]/div/div[2]/div/h2")
	private WebElement biologicsParenterals;
	public WebElement getBiologicsParenterals(){
		return biologicsParenterals;
	}
	
	@FindBys({
		@FindBy(className = "item_2_con"),
		@FindBy(tagName = "ul")
	})
	private List<WebElement> allBiologicsParenterals;
	public List<WebElement> getAllBiologicsParenterals()
	{
		return allBiologicsParenterals;
	}
	
	@FindBy(xpath = "//div[@id='wrap-phases']/div[2]/div/div[3]/div/h2")
	private WebElement inhalationNasal;
	public WebElement getInhalationNasal(){
		return inhalationNasal;
	}
	
	@FindBys({
		@FindBy(className = "item_3_con"),
		@FindBy(tagName = "ul")
	})
	private List<WebElement> allInhalationNasal;
	public List<WebElement> getAllInhalationNasal()
	{
		return allInhalationNasal;
	}
	
	@FindBys({
		@FindBy(className = "offerings-title"),
		@FindBy(tagName= "h2")
		
	})
	private WebElement featuredOfferings;
	public WebElement getFeaturedOfferings()
	{
		return featuredOfferings;
	}
	
	
	@FindBy(className = "offerings-item__title")
	private List<WebElement> allOfferingsItem;
	public List<WebElement> getAllOfferingsItem()
	{
		return allOfferingsItem;
	}
	
	@FindBy(xpath = "//span/a")
	private WebElement seeAllOfferings;
	public WebElement getSeeAllOfferings()
	{
		return seeAllOfferings;
	}
	
	@FindBy(xpath = "//span[2]/a")
	private WebElement downloadBrochureHere;
	public WebElement getDownloadBrochureHere()
	{
		return downloadBrochureHere;
	}
	
	@FindBys({
		@FindBy(className = "challenges-description__button"),
		@FindBy(tagName = "a")
	})
	private WebElement challengeUs;
	public WebElement getChallengeUs()
	{
		return challengeUs;
	}
	
	@FindBy(className = "challenges-content__item--content")
	private List<WebElement> allChallengeUsItem;
	public List<WebElement> getAllChallengeUsItem()
	{
		return allChallengeUsItem;
	}
	
	
}
