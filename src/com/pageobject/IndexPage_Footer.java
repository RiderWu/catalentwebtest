package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class IndexPage_Footer {
public WebDriver driver = null;
	
	public IndexPage_Footer(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "footer")
	private WebElement footer;
	public WebElement getFooter(){
		return footer;
	}
	
	@FindBy(className = "underline")
	private List<WebElement> footerlinks;
	public List<WebElement> getFooterLinks(){
		return footerlinks;
	}
	
	@FindBy(xpath = "//div[2]/div/div[5]/a[1]")
	private WebElement facebook;
	public WebElement getFacebook(){
		return facebook;
	}
	
	@FindBy(xpath = "//div[2]/div/div[5]/a[2]")
	private WebElement twitter;
	public WebElement getTwitter(){
		return twitter;
	}
	
	@FindBy(xpath = "//div[2]/div/div[5]/a[3]")
	private WebElement linkedin;
	public WebElement getLinkedin(){
		return linkedin;
	}
	
	@FindBy(xpath = "//div[2]/div/div[5]/a[4]")
	private WebElement youtube;
	public WebElement getYoutube(){
		return youtube;
	}
	
	@FindBys({
		@FindBy(className = "right"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> copyrightlinks;
	public List<WebElement> getCopyrightLinks(){
		return copyrightlinks;
	}
}
