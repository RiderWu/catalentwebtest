package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class IndexPage_Header {
public WebDriver driver = null;
	
	public IndexPage_Header(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "message-bar")
	private WebElement messageBar;
	public WebElement getMessageBar(){
		return messageBar;
	}
	
	@FindBy(id = "subscribe_submit")
	private WebElement subscribeNow;
	public WebElement getSubscribeNow(){
		return subscribeNow;
	}
	
	@FindBy(className = "close-message")
	private WebElement closeMessage;
	public WebElement getCloseMessage(){
		return closeMessage;
	}
	
	@FindBys({
		@FindBy(className = "main-nav-links"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> mainNavLinks;
	public List<WebElement> getMainNavLinks(){
		return mainNavLinks;
	}
	
	@FindBys({
		@FindBy(className = "searchForm"),
		@FindBy(tagName = "input")
	})
	private WebElement searchTextBox;
	public WebElement getSearchTextBox(){
		return searchTextBox;
	}
	
	@FindBys({
		@FindBy(className = "searchForm"),
		@FindBy(tagName = "button")
	})
	private WebElement searchButton;
	public WebElement getSearchButton(){
		return searchButton;
	}
	
	@FindBy(id = "global-language")
	private WebElement globalButton;
	public WebElement getGlobalButton(){
		return globalButton;
	}
	
	@FindBy(id = "language-div")
	private WebElement globalDiv;
	public WebElement getGlobalDiv(){
		return globalDiv;
	}
	
	@FindBy(className = "lang-close")
	private WebElement globalClose;
	public WebElement getGlobalClose(){
		return globalClose;
	}
	
	@FindBy(className = "logo")
	private WebElement logo;
	public WebElement getLogo(){
		return logo;
	}
	
	@FindBy(className = "nav-menu")
	protected List<WebElement> navMenu;
	public List<WebElement> getNavMenu(){
		return navMenu;
	}
}
