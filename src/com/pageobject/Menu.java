package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Menu extends IndexPage_Header {
public WebDriver driver = null;
	
	public Menu(WebDriver driver){
		super(driver);
	}
	
	public WebElement get(int index) {
		return super.navMenu.get(index);
	}
	
	public int size() {
		return super.navMenu.size();
	}
	
	public WebElement getFirstLevelMenu(int index){
		return super.navMenu.get(index).findElement(By.tagName("a"));
	}
	
	public WebElement getArrowDown(int index){
		try {
			return super.navMenu.get(index).findElement(By.tagName("i"));
		} catch (NoSuchElementException e) {
			return null;
		}
	}
	
	public WebElement getDropDownMenu(int index){
		try {
			return super.navMenu.get(index).findElement(By.tagName("div"));
		} catch (NoSuchElementException e) {
			return null;
		}
	}
	
	public List<WebElement> getSecondLevelMenu(int index){
		return super.navMenu.get(index).findElement(By.tagName("div")).findElements(By.tagName("a"));
	}
}
