package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class Offerings {
public WebDriver driver = null;
	
	public Offerings(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
               
	@FindBy(className = "tab-content")
	protected WebElement TabContent;
	public WebElement getTabContent(){
		return TabContent;
	}
	
	@FindBys({
		@FindBy(className = "kit-g-20"),
		@FindBy(tagName = "li")
	})
	protected List<WebElement> Lists;
	public List<WebElement> getLists(){
		return Lists;
	} 
	
	@FindBys({
		@FindBy(className = "visit-block"),
		@FindBy(tagName = "a")
	})
	protected WebElement VisitOurPublications;
	public WebElement getVisitOurPublications(){
		return VisitOurPublications;
	} 
		
	@FindBys({
		@FindBy(className = "kiwi-hidden-s"),
		@FindBy(tagName = "p")
	})
	protected WebElement LookingFor;
	public WebElement getLookingFor(){
		return LookingFor;
	}
	
	@FindBy(className = "tagged-elem-title")
	protected List<WebElement> Title;
	public List<WebElement> getTitle(){
		return Title;
	}
	
}
