package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.pageobject.BiologicsPage;
import com.pageobject.CarouselOnBiologics;
import com.pageobject.IndexPage_DDDSPage;
import com.pageobject.IndexPage_Footer;
import com.pageobject.IndexPage_Header;
import com.pageobject.Menu;
import com.web.Common;

public class Biologics_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/index.php";

	@Parameters({ "environment1" })
	@BeforeSuite
	public void beforeSuite(@Optional("http://biologics.catalent.com") String environment1) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment1;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_BiologicsPageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-BiologicsPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 28000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_BiologicsPageCarousel() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			
			CarouselOnBiologics carouselOnBiologics = new CarouselOnBiologics(driver);
			Common.scrollTo(carouselOnBiologics.getFirst());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FirstHero");
			
			Common.scrollTo(carouselOnBiologics.getSecond());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-SecondHero");
			
			Common.scrollTo(carouselOnBiologics.getThird());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ThirdHero");
			
			carouselOnBiologics.getNext().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickNext");
			
			carouselOnBiologics.getPrev().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ClickPrev");
			
			Common.scrollTo(carouselOnBiologics.getFirst());
			carouselOnBiologics.getPlayVideo().click();
			
			for(String winHandle:driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickPlayVideo");
			
			driver.close();
			for(String winHandle:driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
			Common.scrollTo(carouselOnBiologics.getSecond());
			carouselOnBiologics.getLearnMore().click();
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-ClickLearnMore");
			driver.navigate().back();
			
			Common.scrollTo(carouselOnBiologics.getThird());
			carouselOnBiologics.getLearnMoreOnSlide3().click();
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-ClickLearnMoreOnSlide3");
			driver.navigate().back();
			
			Common.scrollTo(carouselOnBiologics.getThird());
			carouselOnBiologics.getViewWebInar().click();
			for(String winHandle:driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"09-ClickViewWebInar");
			
			driver.close();
			for(String winHandle:driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
		
			
			Assert.assertTrue(carouselOnBiologics.getFirst().isEnabled(), "FirstHero is not enabled");
			Assert.assertTrue(carouselOnBiologics.getSecond().isEnabled(), "SecondHero is not enabled");
			Assert.assertTrue(carouselOnBiologics.getThird().isEnabled(), "ThirdHero is not enabled");
			Assert.assertTrue(carouselOnBiologics.getNext().isEnabled(), "Next is not enabled");
			Assert.assertTrue(carouselOnBiologics.getPrev().isEnabled(), "Prev is not enabled");
			Assert.assertTrue(carouselOnBiologics.getPlayVideo().isEnabled(), "PlayVideo is not enabled");
			Assert.assertTrue(carouselOnBiologics.getLearnMore().isEnabled(), "LearnMore is not enabled");
			Assert.assertTrue(carouselOnBiologics.getLearnMoreOnSlide3().isEnabled(), "LearnMoreAboutUs is not enabled");
			Assert.assertTrue(carouselOnBiologics.getViewWebInar().isEnabled(), "DrugDevelopmentDeliverySupply is not enabled");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_BiologicsPageArticle() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			BiologicsPage biologicsPage = new BiologicsPage(driver);
			
			Common.scrollTo(biologicsPage.getArticle());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ArticleContent");
			biologicsPage.getClickHere().click();
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickClickHerebutton");
			driver.navigate().back();
			
			//latestNews
			Common.scrollTo(biologicsPage.getLatestNews().get(0));
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-LatestNews");
			
			for (int i=0; i < biologicsPage.getLatestNews().get(0).findElements(By.tagName("li")).size(); i++){
				biologicsPage.getLatestNews().get(0).findElements(By.tagName("li")).get(i).click();
				
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"04-LatestNews"+i);
				
				driver.navigate().back();
			}
			
			biologicsPage.getLatestNews().get(1).click();
			for(String winHandle:driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ContactUs");
			
			driver.close();
			for(String winHandle:driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
		

			Assert.assertTrue(biologicsPage.getArticle().isEnabled(), "Article is not enabled");
			Assert.assertTrue(biologicsPage.getClickHere().isEnabled(), "ClickHere is not enabled");
			Assert.assertTrue(biologicsPage.getLatestNews().get(0).isEnabled(), "LatestNews is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_BiologicsPageBlocks() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			BiologicsPage biologicsPage = new BiologicsPage(driver);
			
			Common.scrollTo(biologicsPage.getBlocks().get(0));
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Blocks");
			
			for (int i=0; i < biologicsPage.getBlocks().size(); i++){
				biologicsPage.getBlocks().get(i).click();				
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-ClickBlocks"+i);
				
				driver.navigate().back();
			}
			
			Assert.assertTrue(biologicsPage.getBlocks().get(0).isEnabled(), "Blocks is not enabled");


		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_BiologicsPageMainNavLinks() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			BiologicsPage biologicsPage = new BiologicsPage(driver);
			Common.scrollTo(0);
			
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-MainNavLink");
			
			String handle = driver.getWindowHandle();
			
			String screenshotname = null;
			String handleTemp=null;
			for(int i=0;i<biologicsPage.getMainNavLinks().size();i++){
				screenshotname = biologicsPage.getMainNavLinks().get(i).getText();
				biologicsPage.getMainNavLinks().get(i).click();
				
				for( String winHandle:driver.getWindowHandles()){
				  //  driver.switchTo().window(winHandle);
					handleTemp = winHandle;
				    
				}
				
				
					if (handleTemp.equals(handle)){
						Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click"+screenshotname);
						driver.navigate().back();
					
					}else{
						for(String winHandle:driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}
						
						Thread.sleep(5000);
						Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
								"02-Click"+screenshotname);
						
						driver.close();
						for(String winHandle:driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}
						
					}
					
				
			}
			
			Assert.assertTrue(null!=biologicsPage.getMainNavLinks(), "MainNavLinks is not existing");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_BiologicsPageMainSearch() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			BiologicsPage biologicsPage = new BiologicsPage(driver);
			
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Search");
			
			biologicsPage.getSearchButton().click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-SearchWithoutInput");
			
			biologicsPage.getSearchTextBox().sendKeys("Catalent");
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-InputSearchTerm");
			
			biologicsPage.getSearchButton().click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-SearchResultsLoaded");
			
			driver.navigate().back();
			
			Assert.assertTrue(biologicsPage.getSearchTextBox().isEnabled(), "SearchTextBox is not enabled");
			Assert.assertTrue(biologicsPage.getSearchButton().isEnabled(), "SearchButton is not enabled");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_BiologicsPageLogo() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			BiologicsPage biologicsPage = new BiologicsPage(driver);
			
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Logo");
			
			biologicsPage.getLogo().click();
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickLogo");
			
			Assert.assertTrue(biologicsPage.getLogo().isEnabled(), "Logo is not enabled");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_BiologicsPageNavMenu() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			BiologicsPage biologicsPage = new BiologicsPage(driver);
			
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-NavLink");
			
			String handle = driver.getWindowHandle();
			
			String screenshotname = null;
			String handleTemp=null;
			for(int i=0;i<biologicsPage.getNavMenu().size();i++){
				screenshotname = biologicsPage.getNavMenu().get(i).getText();
				biologicsPage.getNavMenu().get(i).click();
				
				for( String winHandle:driver.getWindowHandles()){
				  //  driver.switchTo().window(winHandle);
					handleTemp = winHandle;
				    
				}
				
				
					if (handleTemp.equals(handle)){
						Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click"+screenshotname);
						driver.navigate().back();
					
					}else{
						for(String winHandle:driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}
						
						Thread.sleep(5000);
						Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
								"02-Click"+screenshotname);
						
						driver.close();
						for(String winHandle:driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}
						
					}
					
				
			}
			
			Assert.assertTrue(null!=biologicsPage.getNavMenu(), "MainNavLinks is not existing");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_BiologicsPageFooterLinks() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			BiologicsPage biologicsPage = new BiologicsPage(driver);
			
			Common.scrollTo(biologicsPage.getFooter());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FooterLinks");
			
			String handle = driver.getWindowHandle();
			
			String screenshotname = null;
			String handleTemp=null;
			for(int i=0;i<biologicsPage.getFooterLinks().size();i++){
				screenshotname = biologicsPage.getFooterLinks().get(i).getText();
				biologicsPage.getFooterLinks().get(i).click();
				
				for( String winHandle:driver.getWindowHandles()){
					handleTemp = winHandle;
				    
				}
				
				
					if (handleTemp.equals(handle)){
						Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click"+screenshotname);
						driver.navigate().back();
					
					}else{
						for(String winHandle:driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}
						
						Thread.sleep(5000);
						Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
								"02-Click"+screenshotname);
						
						driver.close();
						for(String winHandle:driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}
						
					}
					
				
			}
			
						
			Assert.assertTrue(biologicsPage.getFooter().isEnabled(), "Footer is not enabled");
			Assert.assertTrue(null!=biologicsPage.getFooterLinks()&&0!=biologicsPage.getFooterLinks().size(), "FooterLinks is null or empty");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	public boolean doesWebElementExist(WebElement webElement)
	 { 
	        try { 
	               webElement.isEnabled(); 
	               return true; 
	        } catch (Exception e) { 
	            return false; 
	        } 
	 } 
	
	@Test
	public void Test_BiologicsPageFooterSocialLinks() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			BiologicsPage biologicsPage = new BiologicsPage(driver);
			
			Common.scrollTo(biologicsPage.getFooter());
			
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FooterSocialLinks");
			
			
			if (doesWebElementExist(biologicsPage.getImage())){
				Actions action = new Actions(driver);  
				action.moveToElement(biologicsPage.getImage()).build().perform();
				Thread.sleep(3000);			
				biologicsPage.getcloseIcon().click();
			}
						
			biologicsPage.getFacebook().click();
			Thread.sleep(20000);
			Common.switchToNewTab(driver);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickFacebook");
			driver.close();
			Common.switchBackToInitialTab(driver);
			
			biologicsPage.getTwitter().click();
			Thread.sleep(20000);
			Common.switchToNewTab(driver);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickTwitter");
			driver.close();
			Common.switchBackToInitialTab(driver);
			
			biologicsPage.getLinkedin().click();
			Thread.sleep(20000);
			Common.switchToNewTab(driver);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickLinkedin");
			driver.close();
			Common.switchBackToInitialTab(driver);
			
			biologicsPage.getYoutube().click();
			Thread.sleep(20000);
			Common.switchToNewTab(driver);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ClickYoutube");
			driver.close();
			Common.switchBackToInitialTab(driver);
			
			Assert.assertTrue(biologicsPage.getFooter().isEnabled(), "Footer is not enabled");
			Assert.assertTrue(biologicsPage.getFacebook().isEnabled(), "Facebook is not enabled");
			Assert.assertTrue(biologicsPage.getTwitter().isEnabled(), "Twitter is not enabled");
			Assert.assertTrue(biologicsPage.getLinkedin().isEnabled(), "Linkedin is not enabled");
			Assert.assertTrue(biologicsPage.getYoutube().isEnabled(), "Youtube is not enabled");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_BiologicsPageFooterCopyrightLinks() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			BiologicsPage biologicsPage = new BiologicsPage(driver);
			
			Common.scrollTo(biologicsPage.getFooter());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FooterCopyrightLinks");
			
			String screenshotname = null;
			for(int i=0;i<biologicsPage.getCopyrightLinks().size();i++){
				screenshotname=biologicsPage.getCopyrightLinks().get(i).getText();
				biologicsPage.getCopyrightLinks().get(i).click();
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click "+screenshotname);
				driver.navigate().back();
			}
			
			Assert.assertTrue(biologicsPage.getFooter().isEnabled(), "Footer is not enabled");
			Assert.assertTrue(null!=biologicsPage.getCopyrightLinks()&&0!=biologicsPage.getCopyrightLinks().size(), "CopyrightLinks is null or empty");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	
	
	
}
