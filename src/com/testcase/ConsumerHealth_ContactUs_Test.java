package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.pageobject.ConsumerHealth_ContactUs;
import com.web.Common;


public class ConsumerHealth_ContactUs_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/index.php/Contact-Us";

	@Parameters({ "environment2" })
	@BeforeSuite
	public void beforeSuite(@Optional("http://consumerhealth.catalent.com") String environment2) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment2;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_ContactUsPageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ContactUsPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 24000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test
	public void Test_ContactUs() throws Exception{
		try{
			ConsumerHealth_ContactUs consumerContactUs = new ConsumerHealth_ContactUs (driver);
		     driver.navigate().to(domain + path);
		     
		     Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-Contact");
				    
			//consumerContactUs.getFirstName().sendKeys("test");
			//Thread.sleep(1000);
			//consumerContactUs.getLastName().sendKeys("test");
			//Thread.sleep(1000);
			//consumerContactUs.getCompanyName().sendKeys("test");
			//Thread.sleep(1000);
			consumerContactUs.getPhoneNumber().sendKeys("13354284202");
			Thread.sleep(1000);
			Common.scrollTo(consumerContactUs.getEmail());
			//consumerContactUs.getEmail().sendKeys("test@test.com");
			//Thread.sleep(1000);
			//consumerContactUs.getconfirmEmail().sendKeys("test@test.com");
			//Thread.sleep(1000);
			//consumerContactUs.getRegionList().get(1).click();
			Thread.sleep(1000);
			consumerContactUs.getPurposeList().get(2).click();
			Thread.sleep(1000);
			consumerContactUs.getChallenge().sendKeys("test");
			//Thread.sleep(1000);
			//consumerContactUs.getProductCategory().get(2).click();
			Thread.sleep(1000);
			consumerContactUs.getSubmit().click();
			Thread.sleep(2000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-SubmitResult");
			    
			
			
			Assert.assertEquals(consumerContactUs.getErrorMessage().getText(), "Error, Please see area(s) highlighted in red.");
			
			
		}catch (Exception e){
			
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
	
}}}
