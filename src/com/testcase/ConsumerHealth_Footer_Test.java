package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.ConsumerHealth_Footer;
import com.pageobject.Menu;
import com.web.Common;

public class ConsumerHealth_Footer_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/index.php";

	@Parameters({ "environment2" })
	@BeforeSuite
	public void beforeSuite(@Optional("http://consumerhealth.catalent.com/index.php") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}
	@Test
	public void Test_ConsumerHomePageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ConsumerHomePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 24000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test
	public void Test_NavigationFooter() throws Exception {
		try {
			
			ConsumerHealth_Footer consumerFooter = new ConsumerHealth_Footer (driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(consumerFooter.getFooterNav().get(0));
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Footer");
					
			String navname1 = null;
			for(int i=0; i<3; i++){
				navname1 = consumerFooter.getFooterNav().get(i).getText();
				consumerFooter.getFooterNav().get(i).click();
				Thread.sleep(4000);
				driver.switchTo().window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1]);
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-ClickFooterNav-"+navname1);
				driver.close();
				driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
				
				Assert.assertTrue(consumerFooter.getFooterNav().get(i).isEnabled(),navname1 +  "is not enabled");
			}
			
			String navname2 = null;
			for(int i=3; i<5; i++){
				navname2 = consumerFooter.getFooterNav().get(i).getText();
				consumerFooter.getFooterNav().get(i).click();
				Thread.sleep(4000);
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"03-ClickFooterNav-"+navname2);
				driver.navigate().back();
				
				Assert.assertTrue(consumerFooter.getFooterNav().get(i).isEnabled(), navname2 + "is not enabled");
			}
			
			
			
		}catch (Exception e){
			
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test
	public void Test_FooterShare() throws Exception {
		try {
			
			ConsumerHealth_Footer consumerFooter = new ConsumerHealth_Footer (driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(consumerFooter.getFooterNav().get(0));
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FooterShare");
					
			String sharename = null;
			for(int i=0; i<5; i++){
				sharename = consumerFooter.getFooterShare().get(i).getText();
				consumerFooter.getFooterShare().get(i).click();
				Thread.sleep(4000);
				driver.switchTo().window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1]);
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						i + "ClickFooterShare-"+sharename);
				driver.close();
				driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
				
				Assert.assertTrue(consumerFooter.getFooterShare().get(i).isEnabled(), sharename + "is not enabled");
			}
			   }catch (Exception e){
		e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
			
			@Test
			public void Test_FooterCorporate() throws Exception {
				try {
					
					ConsumerHealth_Footer consumerFooter = new ConsumerHealth_Footer (driver);
					driver.navigate().to(domain + path);
					
					Common.scrollTo(consumerFooter.getFooterNav().get(0));
					Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
							"01-Corporate");
							
					String corporatename = null;
					for(int i=0; i<2; i++){
						corporatename = consumerFooter.getCorporate().get(i).getText();
						consumerFooter.getCorporate().get(i).click();
						Thread.sleep(4000);
						//driver.switchTo().window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1]);
						Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
								i + "ClickCorporate-"+corporatename);
						driver.navigate().back();
						//driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
						
						Assert.assertTrue(consumerFooter.getCorporate().get(i).isEnabled(), corporatename + "is not enabled");
					}
					
					
			
		}catch (Exception e){
			
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
