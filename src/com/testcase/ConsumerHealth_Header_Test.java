package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


import com.pageobject.Menu;
import com.web.Common;
import com.pageobject.ConsumerHealth_Header;

public class ConsumerHealth_Header_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/index.php";
	
	@Parameters({ "environment2" })
	@BeforeSuite
	public void beforeSuite(@Optional("http://consumerhealth.catalent.com") String environment2) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment2;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}
	
	@Test
	public void Test_ConsumerHomePageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ConsumerHomePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 24000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test
	public void Test_ConsumerLogo() throws Exception {
		try {
			
			ConsumerHealth_Header consumerHeader = new ConsumerHealth_Header (driver);
			driver.navigate().to(domain + path);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ConsumerLogo");
			
			consumerHeader.getConsumerLogo().click();
			Thread.sleep(4000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickLogo");

			Assert.assertTrue(consumerHeader.getConsumerLogo().isEnabled(), "ConsumerLogo is not enabled");
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_NavigationHeader() throws Exception {
		try {
			
			ConsumerHealth_Header consumerHeader = new ConsumerHealth_Header (driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(consumerHeader.getConsumerLogo());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Header");
					
			String navname = null;
			for(int i=0; i<4; i++){
				navname = consumerHeader.getNavigation().get(i).getText();
				consumerHeader.getNavigation().get(i).click();
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"03-ClickNav"+navname);
				driver.navigate().back();
				Assert.assertTrue(consumerHeader.getNavigation().get(i).isEnabled(), navname + "is not enabled");
			}
			consumerHeader.getNavigation().get(4).click();
			Thread.sleep(5000);
			driver.switchTo().window((String) driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1]);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickNav"+navname);
			driver.close();
			driver.switchTo().window((String) driver.getWindowHandles().toArray()[0]);
			Assert.assertTrue(consumerHeader.getNavigation().get(4).isEnabled(),  "catalent is not enabled");
			
		}catch (Exception e){
			
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test
	public void Test_NavigationMenu() throws Exception {
		try {
			
			ConsumerHealth_Header consumerHeader = new ConsumerHealth_Header (driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(consumerHeader.getConsumerLogo());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Header");
					
			String navmenuname = null;
			for(int i=0; i<8; i++){
				navmenuname = consumerHeader.getNavigationMenu().get(i).getText();
				consumerHeader.getNavigationMenu().get(i).click();
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"03-ClickNavMenu-"+navmenuname);
				driver.navigate().back();
				
				Assert.assertTrue(consumerHeader.getNavigationMenu().get(i).isEnabled(), navmenuname + "is not enabled");
			}
			
			
		}catch (Exception e){
			
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test
	public void Test_Search() throws Exception {
		try {
			
			ConsumerHealth_Header consumerHeader = new ConsumerHealth_Header (driver);
			driver.navigate().to(domain + path);
			
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Search");
			
			consumerHeader.getSearchButton().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-SearchWithoutInput");
			
			consumerHeader.getSearchBox().sendKeys("consumer");
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-InputSearchText");
			
			consumerHeader.getSearchButton().click();
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-SearchResults");
			driver.navigate().back();
			
			Assert.assertTrue(consumerHeader.getSearchButton().isEnabled(), "SearchButton is not enabled");
			
		}catch (Exception e){
			
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test
	public void Test_Location() throws Exception {
		try {
			
			ConsumerHealth_Header consumerHeader = new ConsumerHealth_Header (driver);
			driver.navigate().to(domain + path);
			
			consumerHeader.getNavigation().get(1).click();
			Thread.sleep(2000);
			String locationname = null;
			for(int i=0; i<4; i++){
				locationname = consumerHeader.getLocationname().get(i).getText();
				consumerHeader.getLocationname().get(i).click();
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-ClickLocation-"+locationname);
				
				Assert.assertTrue(consumerHeader.getLocationname().get(i).isEnabled(), locationname + "is not enabled");		
			}
			
			consumerHeader.getAllLocation().click();
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickAllLocation-"+locationname); 
			
			Assert.assertTrue(consumerHeader.getAllLocation().isEnabled(),  "View All is not enabled");	
			
		}catch (Exception e){
			
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
