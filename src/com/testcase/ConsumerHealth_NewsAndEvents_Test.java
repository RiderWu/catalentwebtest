package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.pageobject.ConsumerHealth_NewsAndEventsPage;
import com.web.Common;

public class ConsumerHealth_NewsAndEvents_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/index.php/news-events";

	@Parameters({ "environment2" })
	@BeforeSuite
	public void beforeSuite(@Optional("http://consumerhealth.catalent.com") String environment2) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment2;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_NewsAndEventsPageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-NewsAndEventsPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 24000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test()
	public void test_Tabs() throws Exception {
		try{
		ConsumerHealth_NewsAndEventsPage newEvents =new ConsumerHealth_NewsAndEventsPage(driver);
		driver.navigate().to(domain + path);
		
		Common.scrollTo(newEvents.getHero());
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), "01-NewsEventsButton");
		
		newEvents.getNews().click();
		Thread.sleep(5000);
		Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"02-ClickNews");
		
		
		newEvents.getEvents().click();
		Thread.sleep(5000);
		Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"03-ClickEvents");
		
		
		
		
		Assert.assertTrue(newEvents.getEvents().isEnabled(), "Events is not enabled");
		Assert.assertTrue(newEvents.getNews().isEnabled(), "News is not enabled");
		
		}catch (Exception e){
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	@Test()
	public void test_ViewAll() throws Exception {
		try{
		ConsumerHealth_NewsAndEventsPage newEvents =new ConsumerHealth_NewsAndEventsPage(driver);
		driver.navigate().to(domain + path);
		
		Common.scrollTo(newEvents.getNews());
		
		newEvents.getNews().click();
		Thread.sleep(1000);
		Common.scrollTo(newEvents.getAllNews());
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"01-AllNewsButton");
		
		newEvents.getAllNews().click();
		Thread.sleep(2000);
		Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"02-ClickAllNews");
		
		driver.navigate().back();
		Common.scrollTo(newEvents.getEvents());
		newEvents.getEvents().click();
		Common.scrollTo(newEvents.getAllEvents());
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"03-AllEventsButton");
		newEvents.getAllEvents().click();
		Thread.sleep(3000);
		Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"04-ClickAllEvents");
		newEvents.getUpcoming().click();
		newEvents.getSeventeen().get(1).click();
		Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
				"05-Seventeen");
		driver.navigate().back();
		driver.navigate().back();
		
		Assert.assertTrue(newEvents.getAllNews().isEnabled(), "AllNews is not enabled");
		
		}catch (Exception e){
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	@Test()
	public void test_Detail() throws Exception {
		try{
		ConsumerHealth_NewsAndEventsPage newEvents =new ConsumerHealth_NewsAndEventsPage(driver);
		driver.navigate().to(domain + path);
		
		
	   Common.scrollTo(newEvents.getNews());
		String newsname=null;
		newEvents.getNews().click();
		for(int i=0;i<6;i++){
			newsname=newEvents.getNewsDetail().get(i).getText();
				newEvents.getNewsDetail().get(i).click();
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					newsname+"NewsDetail");
				driver.navigate().back();
				}
				
	     Assert.assertTrue(newEvents.getNews().isEnabled(), "News is not enabled");
	    
	     Common.scrollTo(newEvents.getEvents());
			String eventsname=null;
			newEvents.getEvents().click();
			for(int i=0;i<6;i++){
				eventsname=newEvents.getEventsDetail().get(i).getText().replace("|", "").replace("-", "");
					newEvents.getEventsDetail().get(i).click();
					Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						eventsname+"EventsDetail");
					driver.navigate().back();
					newEvents.getEvents().click();
					}
					
		     Assert.assertTrue(newEvents.getEvents().isEnabled(), "Events is not enabled");
		}catch (Exception e){
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
