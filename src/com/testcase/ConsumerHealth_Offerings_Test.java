package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.pageobject.ConsumerHealthPage;
import com.pageobject.Offerings;
import com.web.Common;

public class ConsumerHealth_Offerings_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/index.php/offerings";

	@Parameters({ "environment2" })
	@BeforeSuite
	public void beforeSuite(@Optional("http://consumerhealth.catalent.com") String environment2) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment2;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_OfferingsPageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-OfferingsPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 24000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_OfferingsPageBrowseByTopic() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Offerings offerings = new Offerings(driver);
			Common.scrollTo(offerings.getTabContent());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ScrolltoBrowserByTabContent");
			
			String title = null;
			for(int i=0;i<4;i++){
				Common.scrollTo(offerings.getTabContent());
				title = offerings.getLists().get(i).getText();
				offerings.getLists().get(i).click();
				Thread.sleep(1000);
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click "+ title);
				}

			Assert.assertTrue(offerings.getTabContent().isEnabled(), "TabContent is not enabled");
			Assert.assertTrue(offerings.getLists().get(0).isEnabled(), "ViewAll is not enabled");
			Assert.assertTrue(offerings.getLists().get(1).isEnabled(), "OverTheCounter is not enabled");
			Assert.assertTrue(offerings.getLists().get(2).isEnabled(), "Nutritional is not enabled");
			Assert.assertTrue(offerings.getLists().get(3).isEnabled(), "Beauty is not enabled");
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
}
	@Test
	public void Test_VisitOurPublications() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Offerings offerings = new Offerings(driver);
			Common.scrollTo(offerings.getLookingFor());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ScrolltoVisitOurPublications");
			offerings.getVisitOurPublications().click();
			Thread.sleep(2000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-clickVisitOurPublications");
			driver.navigate().back();

			Assert.assertTrue(offerings.getLookingFor().isEnabled(), "LookingForContent is not enabled");
			Assert.assertTrue(offerings.getVisitOurPublications().isEnabled(), "VisitOurPublications is not enabled");
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	@Test
	public void Test_Offerings() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Offerings offerings = new Offerings(driver);
			Common.scrollTo(offerings.getTabContent());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ScrolltoVisitOurPublications");
			
			String title = null;
			for(int i=0;i<15;i++){
				Common.scrollTo(offerings.getTitle().get(i));
				title = offerings.getTitle().get(i).getText();
				offerings.getTitle().get(i).click();
				Thread.sleep(2000);
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click "+ title);
				driver.navigate().back();				
				Assert.assertTrue(offerings.getTabContent().isEnabled(), "LookingForContent is not enabled");
				Assert.assertTrue(offerings.getTitle().get(i).isEnabled(), "Offering title is not enabled");				
			}
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
}
