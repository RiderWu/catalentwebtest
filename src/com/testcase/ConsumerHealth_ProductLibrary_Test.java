package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.gargoylesoftware.htmlunit.javascript.host.dom.Comment;
import com.pageobject.ConsumerHealth_ProductLibraryPage;
import com.web.Common;

public class ConsumerHealth_ProductLibrary_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/index.php/Product-Library";

	@Parameters({ "environment2" })
	@BeforeSuite
	public void beforeSuite(@Optional("http://consumerhealth.catalent.com") String environment2) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment2;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("IE") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
	
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_ProductLibraryPageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ProductLibraryPageLoad");			
			Assert.assertTrue(driver.getPageSource().length() > 24000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());			
		}
	}
	
	@Test
	public void Test_OverTheCounter() throws Exception{
		try {
			ConsumerHealth_ProductLibraryPage ProductLibrary = new ConsumerHealth_ProductLibraryPage(driver);
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-StepOne");	
			Thread.sleep(5000);
			ProductLibrary.getTitle().click();
			ProductLibrary.getStepBtn().get(0).click();
			
			this.Test_Steps("Test_OverTheCounter");
			this.Test_ContactUs();		
		} catch (Exception e) {
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
				
	}
	
	
	@Test
	public void Test_NutritionalSupplements() throws Exception{
		try {
			ConsumerHealth_ProductLibraryPage ProductLibrary = new ConsumerHealth_ProductLibraryPage(driver);
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-StepOne");	
			ProductLibrary.getStepBtn().get(1).click();
			Thread.sleep(10000);
			this.Test_Steps("Test_NutritionalSupplements");		
			
		} catch (Exception e) {
			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
				
	}
	
	
	@Test
	public void Test_BeautyCare() throws Exception{
		try {
			ConsumerHealth_ProductLibraryPage ProductLibrary = new ConsumerHealth_ProductLibraryPage(driver);
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "01-StepOne");	
			Thread.sleep(1000);
			ProductLibrary.getStepBtn().get(2).click();
			Thread.sleep(10000);
			this.Test_Steps("Test_BeautyCare");
			
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
				
	}
	
	
	
	private void Test_Steps(String foldername) throws Exception{
		try {
			ConsumerHealth_ProductLibraryPage ProductLibrary = new ConsumerHealth_ProductLibraryPage(driver);
		//	driver.navigate().to(domain + path);
			
			Common.scrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					foldername, "01-StepTwo" );			
			Common.scrollTo(ProductLibrary.getSelectAllBtn());
			Assert.assertTrue(ProductLibrary.getSelectAllBtn().isEnabled(), "SelectAllBtn on step2 is not enabled");
			Assert.assertTrue(ProductLibrary.getNextBtn().isEnabled(), "NextBtn on step2 is not enabled");
			Assert.assertTrue(ProductLibrary.getBackBtn().isEnabled(), "BackBtn on step2 is not enabled");
			ProductLibrary.getSelectAllBtn().click();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					foldername,"02-SelectAllTheOptions");
			
			ProductLibrary.getNextBtn().click();
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
					startTime, foldername,"03-FilterResults");
			Common.scrollTo(ProductLibrary.getResultPageTitle());
			ProductLibrary.getFilterCategoryBtn().click();
			Thread.sleep(1000);
			ProductLibrary.getFirstFilterItem().click();
			Thread.sleep(1000);
			
			Common.scrollTo(ProductLibrary.getFirstItem());
			Common.scrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					 foldername,"04-ClickFilterItem");	
			Assert.assertTrue(ProductLibrary.getFirstItem().isEnabled(), "First Result Item is not enabled");
			Assert.assertTrue(ProductLibrary.getFirstFilterItem().isEnabled(), "First Filter Item is not enabled");
			Common.scrollTo(ProductLibrary.getFirstItem());
			ProductLibrary.getFirstItem().click();
			Thread.sleep(5000);
			
			if (isElementPresent(By.id("firstname"))) {
				Common.scrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
						 foldername,"05-StepThree");
				ProductLibrary.getSubmitBtn().click();
				Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
						foldername,"06-SubmitWithoutInputting");
				ProductLibrary.getStep3PageTitle().click();
				
				Actions actions = new Actions(driver);	
				actions.sendKeys(ProductLibrary.getFirstName(), "testFirstName").perform();		
				actions.sendKeys(ProductLibrary.getLastName(),"testLastName").perform();			
				actions.sendKeys(ProductLibrary.getCompany(),"testCompanyname").perform();				
				actions.sendKeys(ProductLibrary.getEmail(),"testEmail@test.com").perform();	
			//	ProductLibrary.getFirstName().sendKeys("testFirstName");
			//	ProductLibrary.getLastName().sendKeys("testLastName");
			//	ProductLibrary.getCompany().sendKeys("testCompanyname");
			//	ProductLibrary.getEmail().sendKeys("testEmail@test.com");
				Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
						 foldername,"07-SubmitWithInputting");
				ProductLibrary.getSubmitBtn().click();
				Common.scrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
						 foldername,"08-ClickSubmit");
							
			}
			
			else  {
				Common.scrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
						foldername,"05-ResultsDetails");			
				
			} 
							

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height, startTime,
					Thread.currentThread().getStackTrace()[1].getMethodName(), "99-ERROR");
			Assert.assertTrue(false, e.getMessage());

		}
			
	
	}
	
	private boolean isElementPresent(By by){
	      try{
	          driver.findElement(by);
	          return true;
	      }catch(Exception e){
	          return false;
	      }

	  }
	
    @Test
	public void Test_ContactUs() throws Exception {
		try {
			ConsumerHealth_ProductLibraryPage ProductLibrary = new ConsumerHealth_ProductLibraryPage(driver);
		//	driver.navigate().to(domain + path);
			
          //  this.Test_OverTheCounter();
			ProductLibrary.getContactBtn().click();
			Thread.sleep(2000);
            Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ContactUs");
            ProductLibrary.getContactSubmitBtn().click();
            Common.scrollTo(ProductLibrary.getContactFirstNameBox());
            Thread.sleep(2000);
            Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickWithoutInputting");
            Actions actions = new Actions(driver);	
			actions.sendKeys(ProductLibrary.getContactFirstNameBox(), "testFirstName").perform();	
			actions.sendKeys(ProductLibrary.getContactLastNameBox(), "testLastName").perform();
			actions.sendKeys(ProductLibrary.getContactCompanyBox(), "testCompany").perform();
			actions.sendKeys(ProductLibrary.getContactEmailBox(), "test@test.com").perform();
           // ProductLibrary.getContactFirstNameBox().sendKeys("testFirstName");
          //  ProductLibrary.getContactLastNameBox().sendKeys("testLastName");
          //  ProductLibrary.getContactCompanyBox().sendKeys("testCompany");
         //   ProductLibrary.getContactEmailBox().sendKeys("test@test.com");
            Common.scrollTo(ProductLibrary.getEmail());
            actions.sendKeys(ProductLibrary.getContactConEmailBox(), "test@test.com").perform();
        //    ProductLibrary.getContactConEmailBox().sendKeys("test@test.com");
            Thread.sleep(3000);
            ProductLibrary.getContactRegionBox().click();
            Thread.sleep(3000);
            ProductLibrary.getContactRegionOptionBox().click();
            ProductLibrary.getContactCategoryOptionBox().click();
            
            Select s = new Select(driver.findElement(ById.xpath("//select[@id='purpose']")));
            s.selectByValue("Considering solutions for an immediate need");
       //     ProductLibrary.getContactPurposeBox().click();
       //     ProductLibrary.getContactPurposeOption().click();
            
            Thread.sleep(5000);
            Common.scrollTo(ProductLibrary.getContactFirstNameBox());
            Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-FillInTheForm");
            ProductLibrary.getContactSubmitBtn().click();
            Thread.sleep(2000);
            Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickSubmitBtn");
			
			Assert.assertTrue(driver.getPageSource().length() > 24000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
}
