package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.pageobject.ConsumerHealth_Publications;
import com.web.Common;

public class ConsumerHealth_Publications_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/index.php/publications";

	@Parameters({ "environment2" })
	@BeforeSuite
	public void beforeSuite(@Optional("http://consumerhealth.catalent.com") String environment2) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment2;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_PublicationsPageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-PublicationsPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 24000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	
	@Test
	public void Test_Filter() throws Exception {
		try {
			ConsumerHealth_Publications Publications = new ConsumerHealth_Publications(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(Publications.getFilterTitle());
			Publications.getFilterItems().get(1).click();
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickOverTheCounterBtn");
			
			Common.scrollTo(Publications.getFilterTitle());
			Publications.getFilterItems().get(0).click();
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickViewAllBtn");
			
			Common.scrollTo(Publications.getFilterTitle());
			Publications.getFilterItems().get(2).click();
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickNutritionalSupplementsBtn");
			
			Common.scrollTo(Publications.getFilterTitle());
			Publications.getFilterItems().get(0).click();
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickViewAllBtn");
			Common.scrollTo(Publications.getFilterTitle());
			Publications.getFilterItems().get(3).click();
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ClickBeautyBtn");
		//	Common.scrollTo(Publications.getFilterTitle());
			Publications.getBrowseAllOfferings().click();
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickBrowseAllOfferingsLink");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	
	@Test
	public void Test_Result() throws Exception {
		try {
			ConsumerHealth_Publications Publications = new ConsumerHealth_Publications(driver);
			driver.navigate().to(domain + path);
			
			Common.scrollTo(Publications.getFilterTitle());
			for (int i=0;i<3;i++)
			{
				Publications.getLinkItems().get(i).click();
				Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-ClickLink"+i);
				driver.navigate().back();
				Thread.sleep(1000);
			}
			
			 Common.scrollTo(Publications.getPageItems().get(1));
			Publications.getPageItems().get(1).click();
			Common.scrollTo(Publications.getFilterTitle());
			Common.scrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
					startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), "02-ResultsOnPage2");
			Publications.getPageItems().get(2).click();
			Common.scrollTo(Publications.getFilterTitle());
			Common.scrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
					startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), "03-ResultsOnPage3");
			Publications.getPageItems().get(0).click();
			Common.scrollTo(Publications.getFilterTitle());
			Common.scrollDownAndTakeScreenShot(driver, null, browser + "_" + market + "_" + width + "_" + height,
					startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), "04-ResultsOnPage1");

			Common.scrollTo(Publications.getFilterTitle());
			Publications.getSearchText().sendKeys("consumer");
			
			Publications.getSearchBtn().click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"05-SearchResults");
		//	Common.scrollTo(Publications.getFilterTitle());
		//	Publications.getFilterItems().get(0).click();
			
		
			
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	
}
