package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.pageobject.Carousel;
import com.pageobject.ConsumerHealthPage;
import com.pageobject.IndexPage;
import com.web.Common;
import com.pageobject.ConsumerHealthPageCarousel;

public class ConsumerHealth_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/index.php";

	@Parameters({ "environment2" })
	@BeforeSuite
	public void beforeSuite(@Optional("http://consumerhealth.catalent.com") String environment2) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment2;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_ConsumerHealthPageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ConsumerHealthPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 24000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	//new add testcases
	@Test
	public void Test_ConsumerHealthPageCarousel() throws Exception {
		try {
			driver.navigate().to(domain + path);
			
			ConsumerHealthPageCarousel carousConsumerHealthPageCarousel = new ConsumerHealthPageCarousel(driver);
			Common.scrollTo(carousConsumerHealthPageCarousel.getFirst());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FirstHero");
			
			Common.scrollTo(carousConsumerHealthPageCarousel.getSecond());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-SecondHero");
			
			Common.scrollTo(carousConsumerHealthPageCarousel.getThird());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ThirdHero");
			
			carousConsumerHealthPageCarousel.getNext().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickNext");
			
			carousConsumerHealthPageCarousel.getPrev().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ClickPrev");
			
			Common.scrollTo(carousConsumerHealthPageCarousel.getFirst());
			carousConsumerHealthPageCarousel.getPlayVideo().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickPlayVideo");
			driver.navigate().back();
			Common.scrollTo(carousConsumerHealthPageCarousel.getSecond());
			carousConsumerHealthPageCarousel.getExploreLibrary().click();
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-ClickLearnMore");
			driver.navigate().back();
			
			Common.scrollTo(carousConsumerHealthPageCarousel.getThird());
			carousConsumerHealthPageCarousel.getBrowseAll().click();
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-ClickLearnMoreAboutUs");
			driver.navigate().back();
			
			Assert.assertTrue(carousConsumerHealthPageCarousel.getFirst().isEnabled(), "FirstHero is not enabled");
			Assert.assertTrue(carousConsumerHealthPageCarousel.getSecond().isEnabled(), "SecondHero is not enabled");
			Assert.assertTrue(carousConsumerHealthPageCarousel.getThird().isEnabled(), "ThirdHero is not enabled");
			Assert.assertTrue(carousConsumerHealthPageCarousel.getNext().isEnabled(), "Next is not enabled");
			Assert.assertTrue(carousConsumerHealthPageCarousel.getPrev().isEnabled(), "Prev is not enabled");
			Assert.assertTrue(carousConsumerHealthPageCarousel.getPlayVideo().isEnabled(), "PlayVideo is not enabled");
			Assert.assertTrue(carousConsumerHealthPageCarousel.getExploreLibrary().isEnabled(), "LearnMore is not enabled");
			Assert.assertTrue(carousConsumerHealthPageCarousel.getBrowseAll().isEnabled(), "LearnMoreAboutUs is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_ConsumerHealthPageConsumerHealth() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			ConsumerHealthPage consumerHealthPage = new ConsumerHealthPage(driver);
			Common.scrollTo(consumerHealthPage.getConsumerHealthTitle());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ConsumerHealthTitle");	
			
 			consumerHealthPage.getItems().get(0).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-OverTheCounter");
			driver.navigate().back();
			
			Common.scrollTo(consumerHealthPage.getConsumerHealthTitle());		
			consumerHealthPage.getItems().get(1).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-NutritionalSupplements");
			driver.navigate().back();
			
			Common.scrollTo(consumerHealthPage.getConsumerHealthTitle());
			consumerHealthPage.getItems().get(2).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-Beauty");						
			driver.navigate().back(); 
			
			Assert.assertTrue(consumerHealthPage.getConsumerHealthTitle().isEnabled(), "ConsumerHealthTitle is not enabled");
			Assert.assertTrue(consumerHealthPage.getItems().get(0).isEnabled(), "OverTheCounter is not enabled");
			Assert.assertTrue(consumerHealthPage.getItems().get(1).isEnabled(), "NutritionalSupplements is not enabled");
			Assert.assertTrue(consumerHealthPage.getItems().get(2).isEnabled(), "Beauty is not enabled");
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
}
	
}
