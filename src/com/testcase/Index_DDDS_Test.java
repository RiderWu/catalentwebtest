package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.IndexPage_DDDSPage;
import com.web.Common;

public class Index_DDDS_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/index.php/offerings/Interactive-Tool";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("http://www.catalent.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("IE") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_IndexPage_DDDSPageDrugDevelopmentDeliverySupplyPageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-DrugDevelopmentDeliverySupplyPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 100000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_IndexPage_DDDSPageSearch() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage_DDDSPage indexPageDDDSPage = new IndexPage_DDDSPage(driver);
			
			Common.scrollTo(0);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Search");
			
			indexPageDDDSPage.getSearchButton().click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-SearchWithoutInput");
			
			indexPageDDDSPage.getSearchTextBox().sendKeys("Catalent");
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-InputSearchTerm");
			
			indexPageDDDSPage.getSearchButton().click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-SearchResultsLoaded");
			
			driver.navigate().back();
			
			Assert.assertTrue(indexPageDDDSPage.getSearchTextBox().isEnabled(), "SearchTextBox is not enabled");
			Assert.assertTrue(indexPageDDDSPage.getSearchButton().isEnabled(), "SearchButton is not enabled");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}

	
	public boolean doesWebElementExist(WebElement webElement, By selector)
	 { 
	        try { 
	                webElement.findElement(selector); 
	               return true; 
	        } catch (Exception e) { 
	            return false; 
	        } 
	    } 
	
	@Test
	public void Test_IndexPage_DDDSPagePhase() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage_DDDSPage indexPageDDDSPage = new IndexPage_DDDSPage(driver);
			Actions actions = new Actions(driver);
			
			Common.scrollTo(indexPageDDDSPage.getAllPhase());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-AllPhase");
			
			Common.scrollTo(indexPageDDDSPage.getOralDoesForms());
			indexPageDDDSPage.getOralDoesForms().click();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickOralDoesForms");
			
			String imageName;
			for (int i=0; i < indexPageDDDSPage.getAllItemsInOralDoesForms().size(); i++){
				for (int j = 0; j < indexPageDDDSPage.getAllItemsInOralDoesForms().get(i).findElements(By.tagName("li")).size(); j++){
					
					if (doesWebElementExist(indexPageDDDSPage.getAllItemsInOralDoesForms().get(i).findElements(By.tagName("li")).get(j), By.tagName("p"))){
						Common.scrollTo(indexPageDDDSPage.getAllItemsInOralDoesForms().get(i).findElements(By.tagName("li")).get(j).findElement(By.tagName("p")).getLocation().getY()-290);
						imageName=indexPageDDDSPage.getAllItemsInOralDoesForms().get(i).findElements(By.tagName("li")).get(j).findElement(By.tagName("p")).getText();
						indexPageDDDSPage.getAllItemsInOralDoesForms().get(i).findElements(By.tagName("li")).get(j).click();
						Thread.sleep(1000);
						Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
								"03"+imageName);
						actions.moveByOffset(0, 0).click().build().perform();
						Thread.sleep(1000);
					}
	
				}

			}
			
			Common.scrollTo(indexPageDDDSPage.getBiologicsParenterals());
			indexPageDDDSPage.getBiologicsParenterals().click();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickBiologicsParenterals");
			
			for (int i=0; i < indexPageDDDSPage.getAllBiologicsParenterals().size(); i++){
				for (int j = 0; j < indexPageDDDSPage.getAllBiologicsParenterals().get(i).findElements(By.tagName("li")).size(); j++){
					
					if (doesWebElementExist(indexPageDDDSPage.getAllBiologicsParenterals().get(i).findElements(By.tagName("li")).get(j), By.tagName("p"))){
						Common.scrollTo(indexPageDDDSPage.getAllBiologicsParenterals().get(i).findElements(By.tagName("li")).get(j).findElement(By.tagName("p")).getLocation().getY()-290);
						
						imageName=indexPageDDDSPage.getAllBiologicsParenterals().get(i).findElements(By.tagName("li")).get(j).findElement(By.tagName("p")).getText();
						imageName=imageName.replace("/", "");
						indexPageDDDSPage.getAllBiologicsParenterals().get(i).findElements(By.tagName("li")).get(j).click();
						Thread.sleep(1000);
						Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
								"05"+imageName);
						actions.moveByOffset(0, 0).click().build().perform();
						Thread.sleep(1000);
					}
	
				}

			}
			
			
			Common.scrollTo(indexPageDDDSPage.getInhalationNasal());
			indexPageDDDSPage.getInhalationNasal().click();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickInhalationNasal");
			
			for (int i=0; i < indexPageDDDSPage.getAllInhalationNasal().size(); i++){
				for (int j = 0; j < indexPageDDDSPage.getAllInhalationNasal().get(i).findElements(By.tagName("li")).size(); j++){
					
					if (doesWebElementExist(indexPageDDDSPage.getAllInhalationNasal().get(i).findElements(By.tagName("li")).get(j), By.tagName("p"))){
						Common.scrollTo(indexPageDDDSPage.getAllInhalationNasal().get(i).findElements(By.tagName("li")).get(j).findElement(By.tagName("p")).getLocation().getY()-290);
						
						imageName=indexPageDDDSPage.getAllInhalationNasal().get(i).findElements(By.tagName("li")).get(j).findElement(By.tagName("p")).getText();
						imageName=imageName.replace("/", "");
						indexPageDDDSPage.getAllInhalationNasal().get(i).findElements(By.tagName("li")).get(j).click();
						Thread.sleep(1000);
						Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
								"07"+imageName);
						actions.moveByOffset(0, 0).click().build().perform();
						Thread.sleep(1000);
					}
	
				}

			}
			

			
			Assert.assertTrue(indexPageDDDSPage.getAllPhase().isEnabled(), "AllPhase is not enabled");
			Assert.assertTrue(indexPageDDDSPage.getOralDoesForms().isEnabled(), "OralDoesForms is not enabled");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_IndexPage_DDDSPageFeaturedOfferings() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage_DDDSPage indexPageDDDSPage = new IndexPage_DDDSPage(driver);
			Actions action = new Actions(driver);
			
			String imageName;
				
			Common.scrollTo(indexPageDDDSPage.getFeaturedOfferings().getLocation().getY()+68);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FeaturedOfferings");
			
			for (int i=0; i < indexPageDDDSPage.getAllOfferingsItem().size(); i++){
				imageName=indexPageDDDSPage.getAllOfferingsItem().get(i).getText();
				indexPageDDDSPage.getAllOfferingsItem().get(i).click();
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02"+imageName);
				driver.navigate().back();
			
			}
			
			Common.scrollTo(indexPageDDDSPage.getSeeAllOfferings().getLocation().getY()-100);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-SeeAllOfferings");
			indexPageDDDSPage.getSeeAllOfferings().click();
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickSeeAllOfferings");
			
			driver.navigate().back();
			
			if (browser.equals("Chrome") || browser.equals("Firefox") ||  browser.equals("FireFox")){
				Common.scrollTo(indexPageDDDSPage.getDownloadBrochureHere());
				Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"05-DownloadBrochureHere");
				indexPageDDDSPage.getDownloadBrochureHere().click();
				Thread.sleep(3000);
				Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"06-ClickDownloadBrochureHere");
				Thread.sleep(3000);
			}

			Assert.assertTrue(indexPageDDDSPage.getFeaturedOfferings().isEnabled(), "FeaturedOfferings is not enabled");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_IndexPage_DDDSPageChallengeUs() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage_DDDSPage indexPageDDDSPage = new IndexPage_DDDSPage(driver);
			
			String imageName;
			Common.scrollTo(indexPageDDDSPage.getChallengeUs());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ChallengeUs");	
			indexPageDDDSPage.getChallengeUs().click();
			Thread.sleep(1000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickChallengeUs");
			driver.navigate().back();
			
			for (int i=0; i < 12; i++){
				Common.scrollTo(indexPageDDDSPage.getAllChallengeUsItem().get(i));
				imageName=indexPageDDDSPage.getAllChallengeUsItem().get(i).findElements(By.tagName("p")).get(1).getText();
				Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"03"+imageName);	
				indexPageDDDSPage.getAllChallengeUsItem().get(i).click();
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"03-Click"+imageName);
				driver.navigate().back();
			}
			
			
			Assert.assertTrue(indexPageDDDSPage.getFeaturedOfferings().isEnabled(), "FeaturedOfferings is not enabled");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}

	
}
