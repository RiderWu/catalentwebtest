package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.IndexPage_Footer;
import com.web.Common;

public class Index_Footer_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("http://www.catalent.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}
	
	@Test
	public void Test_FooterLinks() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage_Footer indexpagefooter = new IndexPage_Footer(driver);
			
			Common.scrollTo(indexpagefooter.getFooter());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FooterLinks");
			
			String screenshotname = null;
			for(int i=0;i<indexpagefooter.getFooterLinks().size();i++){
				screenshotname=indexpagefooter.getFooterLinks().get(i).getText();
				indexpagefooter.getFooterLinks().get(i).click();
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click "+screenshotname);
				driver.navigate().back();
			}
			
			Assert.assertTrue(indexpagefooter.getFooter().isEnabled(), "Footer is not enabled");
			Assert.assertTrue(null!=indexpagefooter.getFooterLinks()&&0!=indexpagefooter.getFooterLinks().size(), "FooterLinks is null or empty");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_FooterSocialLinks() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage_Footer indexpagefooter = new IndexPage_Footer(driver);
			
			Common.scrollTo(indexpagefooter.getFooter());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FooterSocialLinks");
			
			indexpagefooter.getFacebook().click();
			Thread.sleep(20000);
			Common.switchToNewTab(driver);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickFacebook");
			driver.close();
			Common.switchBackToInitialTab(driver);
			
			indexpagefooter.getTwitter().click();
			Thread.sleep(20000);
			Common.switchToNewTab(driver);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickTwitter");
			driver.close();
			Common.switchBackToInitialTab(driver);
			
			indexpagefooter.getLinkedin().click();
			Thread.sleep(20000);
			Common.switchToNewTab(driver);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickLinkedin");
			driver.close();
			Common.switchBackToInitialTab(driver);
			
			indexpagefooter.getYoutube().click();
			Thread.sleep(20000);
			Common.switchToNewTab(driver);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickYoutube");
			driver.close();
			Common.switchBackToInitialTab(driver);
			
			Assert.assertTrue(indexpagefooter.getFooter().isEnabled(), "Footer is not enabled");
			Assert.assertTrue(indexpagefooter.getFacebook().isEnabled(), "Facebook is not enabled");
			Assert.assertTrue(indexpagefooter.getTwitter().isEnabled(), "Twitter is not enabled");
			Assert.assertTrue(indexpagefooter.getLinkedin().isEnabled(), "Linkedin is not enabled");
			Assert.assertTrue(indexpagefooter.getYoutube().isEnabled(), "Youtube is not enabled");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_FooterCopyrightLinks() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage_Footer indexpagefooter = new IndexPage_Footer(driver);
			
			Common.scrollTo(indexpagefooter.getFooter());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FooterCopyrightLinks");
			
			String screenshotname = null;
			for(int i=0;i<indexpagefooter.getCopyrightLinks().size();i++){
				screenshotname=indexpagefooter.getCopyrightLinks().get(i).getText();
				indexpagefooter.getCopyrightLinks().get(i).click();
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click "+screenshotname);
				driver.navigate().back();
			}
			
			Assert.assertTrue(indexpagefooter.getFooter().isEnabled(), "Footer is not enabled");
			Assert.assertTrue(null!=indexpagefooter.getCopyrightLinks()&&0!=indexpagefooter.getCopyrightLinks().size(), "CopyrightLinks is null or empty");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
}
