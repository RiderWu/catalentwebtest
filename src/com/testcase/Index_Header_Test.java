package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.pageobject.IndexPage_Header;
import com.pageobject.Menu;
import com.web.Common;

public class Index_Header_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/index.php";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("http://www.catalent.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}
	
	
	@Test
	public void Test_MessageBar() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage_Header indexpageheader = new IndexPage_Header(driver);
			
			Common.scrollTo(indexpageheader.getMessageBar());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-MessageBar");
			
			indexpageheader.getSubscribeNow().click();
			Common.switchToNewTab(driver);
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickSubscribeNow");
			
			driver.close();
			Common.switchBackToInitialTab(driver);
			
			indexpageheader.getCloseMessage().click();
			Thread.sleep(2000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickCloseMessage");
			
			Assert.assertTrue(null!=indexpageheader.getMessageBar(), "MessageBar can not close");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_MainNavLinks() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage_Header indexpageheader = new IndexPage_Header(driver);
			
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-MainNavLink");
			
			String screenshotname = null;
			for(int i=0;i<indexpageheader.getMainNavLinks().size();i++){
				screenshotname = indexpageheader.getMainNavLinks().get(i).getText();
				indexpageheader.getMainNavLinks().get(i).click();
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click"+screenshotname);
				driver.navigate().back();
			}
			
			Assert.assertTrue(null!=indexpageheader.getMainNavLinks(), "MainNavLinks is not existing");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_Search() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage_Header indexpageheader = new IndexPage_Header(driver);
			
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Search");
			
			indexpageheader.getSearchButton().click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-SearchWithoutInput");
			
			indexpageheader.getSearchTextBox().sendKeys("Catalent");
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-InputSearchTerm");
			
			indexpageheader.getSearchButton().click();
			Thread.sleep(10000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-SearchResultsLoaded");
			
			driver.navigate().back();
			
			Assert.assertTrue(indexpageheader.getSearchTextBox().isEnabled(), "SearchTextBox is not enabled");
			Assert.assertTrue(indexpageheader.getSearchButton().isEnabled(), "SearchButton is not enabled");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_Global() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage_Header indexpageheader = new IndexPage_Header(driver);
			
			indexpageheader.getGlobalButton().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickGlobalButton");
			
			String screenshotname = null;
			for(int i=0;i<indexpageheader.getGlobalDiv().findElements(By.tagName("a")).size();i++){
				screenshotname = indexpageheader.getGlobalDiv().findElements(By.tagName("a")).get(i).getText();
				indexpageheader.getGlobalDiv().findElements(By.tagName("a")).get(i).click();
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click "+screenshotname);
				driver.navigate().back();
				if(!indexpageheader.getGlobalDiv().isDisplayed()){
					indexpageheader.getGlobalButton().click();
					Thread.sleep(2000);
				}
			}
			
			indexpageheader.getGlobalClose().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickGlobalClose");
			
			Assert.assertTrue(indexpageheader.getGlobalButton().isEnabled(), "GlobalButton is not enabled");
			Assert.assertTrue(!indexpageheader.getGlobalDiv().isDisplayed(), "GlobalDiv is displayed after closed");
			Assert.assertTrue(!indexpageheader.getGlobalClose().isDisplayed(), "GlobalClose is displayed after closed");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_Logo() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage_Header indexpageheader = new IndexPage_Header(driver);
			
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Logo");
			
			indexpageheader.getLogo().click();
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickLogo");
			
			Assert.assertTrue(indexpageheader.getLogo().isEnabled(), "Logo is not enabled");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_NavMenu() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Menu menu = new Menu(driver);
			
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-NavMenu");
			
			String screenshotname = null;
			for(int i=0;i<menu.size();){
				screenshotname = menu.getFirstLevelMenu(i).getText();
				menu.getFirstLevelMenu(i).click();
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-Click "+screenshotname);
				driver.navigate().back();
				if(null == menu.getArrowDown(i)){
					i++;
				}
				else{
					menu.getArrowDown(i).click();
					Thread.sleep(2000);
					Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
							"03-Click "+screenshotname+" ArrowDown");
					
					for(int j=0;j<menu.getSecondLevelMenu(i).size();j++){
						if(!menu.getDropDownMenu(i).getAttribute("style").equals("display: block;")){
							menu.getArrowDown(i).click();
							Thread.sleep(2000);
						}
						screenshotname = menu.getSecondLevelMenu(i).get(j).getText().replace('/', '-');
						if(menu.getSecondLevelMenu(i).get(j).getAttribute("target").equals("_blank")){
							menu.getSecondLevelMenu(i).get(j).click();
							Thread.sleep(10000);
							Common.switchToNewTab(driver);
							Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
									"04-Click "+screenshotname);
							driver.close();
							Common.switchBackToInitialTab(driver);
						}
						else{
							menu.getSecondLevelMenu(i).get(j).click();
							Thread.sleep(3000);
							Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
									"04-Click "+screenshotname);
							driver.navigate().back();
						}
					}
					
					i++;
				}
				
				
			}
			
			Assert.assertTrue(null != menu && 0 != menu.size(), "MainNav is not null or empty");
			
		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
}
