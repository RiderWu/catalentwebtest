package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.web.Common;
import com.pageobject.Carousel;
import com.pageobject.IndexPage;

public class Index_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;
	private static String path = "/index.php";

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("http://www.catalent.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("IE") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", width, height);
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test
	public void Test_IndexPageLoad() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-HomePageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 60000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_IndexPageCarousel() throws Exception {
		try {
			driver.navigate().to(domain + path);
			
			Carousel carousel = new Carousel(driver);
			Common.scrollTo(carousel.getFirst());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FirstHero");
			
			Common.scrollTo(carousel.getSecond());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-SecondHero");
			
			Common.scrollTo(carousel.getThird());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ThirdHero");
			
			carousel.getNext().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickNext");
			
			carousel.getPrev().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ClickPrev");
			
			Common.scrollTo(carousel.getFirst());
			carousel.getPlayVideo().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickPlayVideo");
			
			carousel.getCloseVideo().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-ClickCloseVideo");
			
			Common.scrollTo(carousel.getSecond());
			carousel.getLearnMore().click();
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-ClickLearnMore");
			driver.navigate().back();
			
			Common.scrollTo(carousel.getThird());
			carousel.getLearnMoreAboutUs().click();
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"09-ClickLearnMoreAboutUs");
			driver.navigate().back();
			
			Common.scrollTo(carousel.getFirst());
			carousel.getDrugDevelopmentDeliverySupply().click();
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"10-ClickDrugDevelopmentDeliverySupply");
			driver.navigate().back();
			
			Common.scrollTo(carousel.getFirst());
			carousel.getBiologics().click();
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"11-ClickBiologics");
			driver.navigate().back();
			
			Common.scrollTo(carousel.getFirst());
			carousel.getConsumerHealth().click();
			Thread.sleep(5000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"12-ClickConsumerHealth");
			driver.navigate().back();
			
			Assert.assertTrue(carousel.getFirst().isEnabled(), "FirstHero is not enabled");
			Assert.assertTrue(carousel.getSecond().isEnabled(), "SecondHero is not enabled");
			Assert.assertTrue(carousel.getThird().isEnabled(), "ThirdHero is not enabled");
			Assert.assertTrue(carousel.getNext().isEnabled(), "Next is not enabled");
			Assert.assertTrue(carousel.getPrev().isEnabled(), "Prev is not enabled");
			Assert.assertTrue(carousel.getPlayVideo().isEnabled(), "PlayVideo is not enabled");
			Assert.assertTrue(carousel.getLearnMore().isEnabled(), "LearnMore is not enabled");
			Assert.assertTrue(carousel.getLearnMoreAboutUs().isEnabled(), "LearnMoreAboutUs is not enabled");
			Assert.assertTrue(carousel.getDrugDevelopmentDeliverySupply().isEnabled(), "DrugDevelopmentDeliverySupply is not enabled");
			Assert.assertTrue(carousel.getBiologics().isEnabled(), "Biologics is not enabled");
			Assert.assertTrue(carousel.getConsumerHealth().isEnabled(), "ConsumerHealth is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_IndexPageLatestNews() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage indexpage = new IndexPage(driver);
			Common.scrollTo(indexpage.getLatestNews());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-LatestNews");
			
			indexpage.getlatestNewsLink().click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickLatestNews");
			driver.navigate().back();
			
			Assert.assertTrue(indexpage.getLatestNews().isEnabled(), "LatestNews is not enabled");
			Assert.assertTrue(indexpage.getlatestNewsLink().isEnabled(), "LatestNewsLink is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_IndexPageBlocks() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage indexpage = new IndexPage(driver);
			
			Common.scrollTo(indexpage.getFirstBlock());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-FirstBlock");
			
			indexpage.getFirstBlock().findElements(By.tagName("a")).get(0).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickFirstBlockImage");
			driver.navigate().back();
			Thread.sleep(3000);
			
			indexpage.getFirstBlock().findElements(By.tagName("a")).get(1).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickFirstBlockContent");
			driver.navigate().back();
			Thread.sleep(3000);
			
			indexpage.getFirstBlock().findElements(By.tagName("a")).get(2).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickFirstBlockLearnMore");
			driver.navigate().back();
			Thread.sleep(3000);
			
			Common.scrollTo(indexpage.getSecondBlock());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-SecondBlock");
			
			indexpage.getSecondBlock().findElements(By.tagName("a")).get(0).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickSecondBlockLeftContent");
			driver.navigate().back();
			Thread.sleep(3000);
			
			indexpage.getSecondBlock().findElements(By.tagName("a")).get(1).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-ClickSecondBlockImage");
			driver.navigate().back();
			Thread.sleep(3000);
			
			indexpage.getSecondBlock().findElements(By.tagName("a")).get(2).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-ClickSecondBlockRightContent");
			driver.navigate().back();
			Thread.sleep(3000);
			
			Common.scrollTo(indexpage.getThirdBlock());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"09-ThirdBlock");
			
			indexpage.getThirdBlock().findElements(By.tagName("a")).get(0).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"10-ClickThirdBlockLeftContent");
			driver.navigate().back();
			Thread.sleep(3000);
			
			indexpage.getThirdBlock().findElements(By.tagName("a")).get(1).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"11-ClickThirdBlockMiddleContent");
			driver.navigate().back();
			Thread.sleep(3000);
			
			indexpage.getThirdBlock().findElements(By.tagName("a")).get(2).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"12-ClickThirdBlockRightMap");
			driver.navigate().back();
			Thread.sleep(3000);
			
			indexpage.getThirdBlock().findElements(By.tagName("a")).get(3).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"13-ClickThirdBlockMiddleSeeAllOfOurLocations");
			driver.navigate().back();
			Thread.sleep(3000);
			
			indexpage.getThirdBlock().findElements(By.tagName("a")).get(4).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"14-ClickThirdBlockMiddleChina");
			driver.navigate().back();
			Thread.sleep(3000);
			
			indexpage.getThirdBlock().findElements(By.tagName("a")).get(5).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"15-ClickThirdBlockMiddleJapan");
			driver.navigate().back();
			Thread.sleep(3000);
			
			indexpage.getThirdBlock().findElements(By.tagName("a")).get(6).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"16-ClickThirdBlockMiddleSpanish");
			driver.navigate().back();
			Thread.sleep(3000);
			
			indexpage.getThirdBlock().findElements(By.tagName("a")).get(7).click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"17-ClickThirdBlockMiddlePortuguese");
			driver.navigate().back();
			Thread.sleep(3000);
			
			Assert.assertTrue(indexpage.getFirstBlock().isEnabled(), "FirstBlock is not enabled");
			Assert.assertTrue(indexpage.getSecondBlock().isEnabled(), "SecondBlock is not enabled");
			Assert.assertTrue(indexpage.getThirdBlock().isEnabled(), "ThirdBlock is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_IndexPageNews() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage indexpage = new IndexPage(driver);
			Common.scrollTo(indexpage.getNewsTitle());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-News");
			
			for(int i=0;i<indexpage.getNewsContent().findElements(By.tagName("a")).size();i++){
				indexpage.getNewsContent().findElements(By.tagName("a")).get(i).click();
				Thread.sleep(3000);
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-ClickNewsLink"+(i+1)+"-");
				driver.navigate().back();
			}
			
			indexpage.getNewsViewAll().click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickNewsViewAll");
			driver.navigate().back();
			
			Assert.assertTrue(indexpage.getNewsTitle().isEnabled(), "NewsTitle is not enabled");
			Assert.assertTrue(indexpage.getNewsContent().isEnabled(), "NewsContent is not enabled");
			Assert.assertTrue(indexpage.getNewsViewAll().isEnabled(), "NewsViewAll is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_IndexPageEvents() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage indexpage = new IndexPage(driver);
			Common.scrollTo(indexpage.getEventsTitle());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Events");
			
			for(int i=0;i<indexpage.getEventsContent().findElements(By.tagName("a")).size();i++){
				indexpage.getEventsContent().findElements(By.tagName("a")).get(i).click();
				Thread.sleep(3000);
				Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"02-ClickEventsLink"+(i+1)+"-");
				driver.navigate().back();
			}
			
			indexpage.getEventsViewAll().click();
			Thread.sleep(3000);
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickEventsViewAll");
			driver.navigate().back();
			
			Assert.assertTrue(indexpage.getEventsTitle().isEnabled(), "EventsTitle is not enabled");
			Assert.assertTrue(indexpage.getEventsContent().isEnabled(), "EventsContent is not enabled");
			Assert.assertTrue(indexpage.getEventsViewAll().isEnabled(), "EventsViewAll is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
	
	@Test
	public void Test_IndexPageTwitter() throws Exception {
		try {
			
			driver.navigate().to(domain + path);
			IndexPage indexpage = new IndexPage(driver);
			Common.scrollTo(indexpage.getTwitterTitle());
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Twitter");
			
			for(int i=0,k=1;i<indexpage.getTwitterItems().size();i++){
				for(int j=0;j<indexpage.getTwitterItems().get(i).findElements(By.tagName("a")).size();j++){
					if(indexpage.getTwitterItems().get(i).findElements(By.tagName("a")).get(j).getAttribute("target").equals("_blank")){
						indexpage.getTwitterItems().get(i).findElements(By.tagName("a")).get(j).click();
						Thread.sleep(20000);
						driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
						Thread.sleep(5000);
						Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
								"02-ClickTwitterLink"+k);
						driver.close();
						driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
					}
					else{
						indexpage.getTwitterItems().get(i).findElements(By.tagName("a")).get(j).click();
						Thread.sleep(5000);
						Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
								"02-ClickTwitterLink"+k);
						driver.navigate().back();
					}
					k++;
					Thread.sleep(5000);
				}
			}
			
			indexpage.getTwitterFollowUs().click();
			Thread.sleep(15000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Common.scrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickTwitterFollowUs");
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			
			Assert.assertTrue(indexpage.getTwitterTitle().isEnabled(), "TwitterTitle is not enabled");
			Assert.assertTrue(!indexpage.getTwitterItems().isEmpty(), "TwitterItems is empty");
			Assert.assertTrue(indexpage.getTwitterFollowUs().isEnabled(), "TwitterFollowUs is not enabled");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
			
		}
	}
}
