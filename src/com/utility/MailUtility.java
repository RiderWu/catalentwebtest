package com.utility;

import java.io.FileOutputStream;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailUtility {

    public static void sendMail() throws Exception {
        
        Properties prop = new Properties();
        prop.setProperty("mail.host", "smtp.163.com");
        prop.setProperty("mail.transport.protocol", "smtp");
        prop.setProperty("mail.smtp.auth", "true");
        Session session = Session.getInstance(prop);
        session.setDebug(true);
        Transport ts = session.getTransport();
        ts.connect("smtp.163.com", "<username>", "<password>");
        Message message = createMail(session);
        ts.sendMessage(message, message.getAllRecipients());
        ts.close();
    }
    
    public static MimeMessage createMail(Session session) throws Exception {
        
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress("from@163.com"));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress("to@163.com"));
        message.setSubject("Automation Test Report");

        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText("Please refer to attachments.");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        
        DataSource source = null;
        String filepath = null;
        
        messageBodyPart = new MimeBodyPart();
        filepath = "test-output/html/index.html";
        source = new FileDataSource(filepath);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName("index.html");
        multipart.addBodyPart(messageBodyPart);
        
        messageBodyPart = new MimeBodyPart();
        filepath = "test-output/html/overview.html";
        source = new FileDataSource(filepath);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName("overview.html");
        multipart.addBodyPart(messageBodyPart);
        
        messageBodyPart = new MimeBodyPart();
        filepath = "test-output/html/reportng.css";
        source = new FileDataSource(filepath);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName("reportng.css");
        multipart.addBodyPart(messageBodyPart);
        
        messageBodyPart = new MimeBodyPart();
        filepath = "test-output/html/reportng.js";
        source = new FileDataSource(filepath);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName("reportng.js");
        multipart.addBodyPart(messageBodyPart);
        
        messageBodyPart = new MimeBodyPart();
        filepath = "test-output/html/sorttable.js";
        source = new FileDataSource(filepath);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName("sorttable.js");
        multipart.addBodyPart(messageBodyPart);
        
        messageBodyPart = new MimeBodyPart();
        filepath = "test-output/html/suite1_test1_results.html";
        source = new FileDataSource(filepath);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName("suite1_test1_results.html");
        multipart.addBodyPart(messageBodyPart);
        
        messageBodyPart = new MimeBodyPart();
        filepath = "test-output/html/suites.html";
        source = new FileDataSource(filepath);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName("suites.html");
        multipart.addBodyPart(messageBodyPart);

        message.setContent(multipart);
        message.saveChanges();

        message.writeTo(new FileOutputStream("mail-report/AutomationTestReport.eml"));
        
        return message;
    }
    
}
